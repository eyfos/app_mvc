-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 08 Juin 2017 à 12:15
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `app_mvc`
--

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `commentId` int(11) NOT NULL,
  `articleId` int(11) NOT NULL,
  `author` varchar(255) CHARACTER SET utf8 NOT NULL,
  `content` text CHARACTER SET utf8 NOT NULL,
  `date` datetime NOT NULL,
  `commentParent` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `comments`
--

INSERT INTO `comments` (`commentId`, `articleId`, `author`, `content`, `date`, `commentParent`) VALUES
(47, 48, 'admin', '<p>5</p>', '2017-05-29 19:38:40', NULL),
(48, 48, 'admin', '<p>6</p>', '2017-05-29 19:38:42', NULL),
(33, 46, 'admin ', 'sdfsd', '2017-05-29 03:47:10', 32),
(34, 46, 'admin', '<p>cxcx</p>', '2017-05-29 03:47:14', NULL),
(43, 48, 'admin', '<p>1</p>', '2017-05-29 19:38:27', NULL),
(44, 48, 'admin', '<p>2</p>', '2017-05-29 19:38:31', NULL),
(45, 48, 'admin', '<p>3</p>', '2017-05-29 19:38:34', NULL),
(46, 48, 'admin', '<p>4</p>', '2017-05-29 19:38:36', NULL),
(49, 48, 'admin', '<p>7</p>', '2017-05-29 19:38:45', NULL),
(51, 48, 'admin ', '<p>&eacute;&eacute;&eacute;&eacute;</p>', '2017-05-29 19:43:50', 43),
(52, 58, 'admin', '<p>fzaf</p>', '2017-05-30 17:15:04', NULL),
(53, 58, 'pseudo', 'sdfdsfs', '2017-05-30 17:16:03', 52),
(54, 58, 'admin', 'admin', '2017-05-30 17:46:08', 53),
(55, 59, 'admin', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lacus dolor, scelerisque sed magna a, lacinia malesuada orci. Sed iaculis, est ac scelerisque rutrum, urna nisi laoreet mauris, et eleifend diam arcu quis turpis. Ut efficitur ex lacus, bibendum tincidunt lectus feugiat id. Vivamus congue, mauris sed molestie tempus, lorem tellus sodales mi, non ultricies sem dui id ante. Integer eleifend tortor magna, in facilisis diam hendrerit eget. Curabitur tempor sodales mi sit amet interdum. Quisque purus odio, lacinia sed elementum vel, bibendum nec magna. Duis sit amet tortor bibendum, fringilla tortor eget, malesuada tortor. Aliquam diam justo, congue nec fermentum a, commodo at lorem. Aliquam dapibus elit quis sem fringilla pharetra et eu ante. Nunc finibus, odio ac facilisis mattis, tortor metus faucibus massa, quis congue massa tortor id ante. Nam lacinia quam quis ullamcorper facilisis. Integer libero sapien, interdum quis tortor id, dictum ornare odio. Nam fringilla imperdiet lacus, quis suscipit libero egestas sit amet.</p>', '2017-05-30 20:03:17', NULL),
(56, 59, 'Bob', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lacus dolor, scelerisque sed magna a, lacinia malesuada orci. Sed iaculis, est ac scelerisque rutrum, urna nisi laoreet mauris, et eleifend diam arcu quis turpis. Ut efficitur ex lacus, bibendum tincidunt lectus feugiat id. Vivamus congue, mauris sed molestie tempus, lorem tellus sodales mi, non ultricies sem dui id ante. Integer eleifend tortor magna, in facilisis diam hendrerit eget. Curabitur tempor sodales mi sit amet interdum. Quisque purus odio, lacinia sed elementum vel, bibendum nec magna. Duis sit amet tortor bibendum, fringilla tortor eget, malesuada tortor. Aliquam diam justo, congue nec fermentum a, commodo at lorem. Aliquam dapibus elit quis sem fringilla pharetra et eu ante. Nunc finibus, odio ac facilisis mattis, tortor metus faucibus massa, quis congue massa tortor id ante. Nam lacinia quam quis ullamcorper facilisis. Integer libero sapien, interdum quis tortor id, dictum ornare odio. Nam fringilla imperdiet lacus, quis suscipit libero egestas sit amet.</p>', '2017-05-30 20:06:51', NULL),
(58, 59, 'admin ', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lacus dolor, scelerisque sed magna a, lacinia malesuada orci. Sed iaculis, est ac scelerisque rutrum, urna nisi laoreet mauris, et eleifend diam arcu quis turpis. Ut efficitur ex lacus, bibendum tincidunt lectus feugiat id. Vivamus congue, mauris sed molestie tempus, lorem tellus sodales mi, non ultricies sem dui id ante. Integer eleifend tortor magna, in facilisis diam hendrerit eget. Curabitur tempor sodales mi sit amet interdum. Quisque purus odio, lacinia sed elementum vel, bibendum nec magna. Duis sit amet tortor bibendum, fringilla tortor eget, malesuada tortor. Aliquam diam justo, congue nec fermentum a, commodo at lorem. Aliquam dapibus elit quis sem fringilla pharetra et eu ante. Nunc finibus, odio ac facilisis mattis, tortor metus faucibus massa, quis congue massa tortor id ante. Nam lacinia quam quis ullamcorper facilisis. Integer libero sapien, interdum quis tortor id, dictum ornare odio. Nam fringilla imperdiet lacus, quis suscipit libero egestas sit amet.</p>', '2017-06-02 15:40:18', 56),
(59, 59, 'Bob', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lacus dolor, scelerisque sed magna a, lacinia malesuada orci. Sed iaculis, est ac scelerisque rutrum, urna nisi laoreet mauris, et eleifend diam arcu quis turpis. Ut efficitur ex lacus, bibendum tincidunt lectus feugiat id. Vivamus congue, mauris sed molestie tempus, lorem tellus sodales mi, non ultricies sem dui id ante. Integer eleifend tortor magna, in facilisis diam hendrerit eget. Curabitur tempor sodales mi sit amet interdum. Quisque purus odio, lacinia sed elementum vel, bibendum nec magna. Duis sit amet tortor bibendum, fringilla tortor eget, malesuada tortor. Aliquam diam justo, congue nec fermentum a, commodo at lorem. Aliquam dapibus elit quis sem fringilla pharetra et eu ante. Nunc finibus, odio ac facilisis mattis, tortor metus faucibus massa, quis congue massa tortor id ante. Nam lacinia quam quis ullamcorper facilisis. Integer libero sapien, interdum quis tortor id, dictum ornare odio. Nam fringilla imperdiet lacus, quis suscipit libero egestas sit amet.</p>', '2017-06-02 15:41:26', 58),
(60, 59, 'admin', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lacus dolor, scelerisque sed magna a, lacinia malesuada orci. Sed iaculis, est ac scelerisque rutrum, urna nisi laoreet mauris, et eleifend diam arcu quis turpis. Ut efficitur ex lacus, bibendum tincidunt lectus feugiat id. Vivamus congue, mauris sed molestie tempus, lorem tellus sodales mi, non ultricies sem dui id ante. Integer eleifend tortor magna, in facilisis diam hendrerit eget. Curabitur tempor sodales mi sit amet interdum. Quisque purus odio, lacinia sed elementum vel, bibendum nec magna. Duis sit amet tortor bibendum, fringilla tortor eget, malesuada tortor. Aliquam diam justo, congue nec fermentum a, commodo at lorem. Aliquam dapibus elit quis sem fringilla pharetra et eu ante. Nunc finibus, odio ac facilisis mattis, tortor metus faucibus massa, quis congue massa tortor id ante. Nam lacinia quam quis ullamcorper facilisis. Integer libero sapien, interdum quis tortor id, dictum ornare odio. Nam fringilla imperdiet lacus, quis suscipit libero egestas sit amet.</p>', '2017-06-02 15:50:47', 58),
(61, 59, 'admin', '<p>Coucou c\'est un test</p>', '2017-06-02 16:25:39', NULL),
(62, 59, 'admin ', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lacus dolor, scelerisque sed magna a, lacinia malesuada orci. Sed iaculis, est ac scelerisque rutrum, urna nisi laoreet mauris, et eleifend diam arcu quis turpis. Ut efficitur ex lacus, bibendum tincidunt lectus feugiat id. Vivamus congue, mauris sed molestie tempus, lorem tellus sodales mi, non ultricies sem dui id ante. Integer eleifend tortor magna, in facilisis diam hendrerit eget. Curabitur tempor sodales mi sit amet interdum. Quisque purus odio, lacinia sed elementum vel, bibendum nec magna. Duis sit amet tortor bibendum, fringilla tortor eget, malesuada tortor. Aliquam diam justo, congue nec fermentum a, commodo at lorem. Aliquam dapibus elit quis sem fringilla pharetra et eu ante. Nunc finibus, odio ac facilisis mattis, tortor metus faucibus massa, quis congue massa tortor id ante. Nam lacinia quam quis ullamcorper facilisis. Integer libero sapien, interdum quis tortor id, dictum ornare odio. Nam fringilla imperdiet lacus, quis suscipit libero egestas sit amet.</p>', '2017-06-02 16:26:04', 56),
(63, 59, 'admin', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lacus dolor, scelerisque sed magna a, lacinia malesuada orci. Sed iaculis, est ac scelerisque rutrum, urna nisi laoreet mauris, et eleifend diam arcu quis turpis. Ut efficitur ex lacus, bibendum tincidunt lectus feugiat id. Vivamus congue, mauris sed molestie tempus, lorem tellus sodales mi, non ultricies sem dui id ante. Integer eleifend tortor magna, in facilisis diam hendrerit eget. Curabitur tempor sodales mi sit amet interdum. Quisque purus odio, lacinia sed elementum vel, bibendum nec magna. Duis sit amet tortor bibendum, fringilla tortor eget, malesuada tortor. Aliquam diam justo, congue nec fermentum a, commodo at lorem. Aliquam dapibus elit quis sem fringilla pharetra et eu ante. Nunc finibus, odio ac facilisis mattis, tortor metus faucibus massa, quis congue massa tortor id ante. Nam lacinia quam quis ullamcorper facilisis. Integer libero sapien, interdum quis tortor id, dictum ornare odio. Nam fringilla imperdiet lacus, quis suscipit libero egestas sit amet.</p>', '2017-06-02 16:35:50', 60),
(64, 59, 'admin', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lacus dolor, scelerisque sed magna a, lacinia malesuada orci. Sed iaculis, est ac scelerisque rutrum, urna nisi laoreet mauris, et eleifend diam arcu quis turpis. Ut efficitur ex lacus, bibendum tincidunt lectus feugiat id. Vivamus congue, mauris sed molestie tempus, lorem tellus sodales mi, non ultricies sem dui id ante. Integer eleifend tortor magna, in facilisis diam hendrerit eget. Curabitur tempor sodales mi sit amet interdum. Quisque purus odio, lacinia sed elementum vel, bibendum nec magna. Duis sit amet tortor bibendum, fringilla tortor eget, malesuada tortor. Aliquam diam justo, congue nec fermentum a, commodo at lorem. Aliquam dapibus elit quis sem fringilla pharetra et eu ante. Nunc finibus, odio ac facilisis mattis, tortor metus faucibus massa, quis congue massa tortor id ante. Nam lacinia quam quis ullamcorper facilisis. Integer libero sapien, interdum quis tortor id, dictum ornare odio. Nam fringilla imperdiet lacus, quis suscipit libero egestas sit amet.</p>', '2017-06-02 16:45:35', 60),
(66, 59, 'zd', '<p>zd</p>', '2017-06-04 02:19:08', NULL),
(67, 59, 'admin', '<p>dsfds</p>', '2017-06-06 03:22:51', NULL),
(68, 59, 'admin', '<p>1111</p>', '2017-06-06 03:23:21', NULL),
(69, 59, 'admin ', '1111', '2017-06-06 03:23:32', 55),
(70, 59, 'admin ', '', '2017-06-06 03:24:13', 55),
(71, 59, 'admin ', '11111', '2017-06-06 03:24:55', 55),
(72, 59, 'admin ', 'gh', '2017-06-06 03:25:32', 55);

-- --------------------------------------------------------

--
-- Structure de la table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `author` varchar(255) NOT NULL,
  `image` varchar(200) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `dateAdd` datetime NOT NULL,
  `dateModif` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `news`
--

INSERT INTO `news` (`id`, `author`, `image`, `title`, `content`, `dateAdd`, `dateModif`) VALUES
(58, 'Jean Forteroche', '215507.jpg', 'Duis sed est pretium, efficitur lacus at, malesuada enim', '<p>Cras rhoncus mattis est, tempus feugiat ante suscipit iaculis. Quisque nec dui mattis, placerat nibh eu, venenatis urna. Nullam facilisis tortor in lacus aliquet, non ultricies dui pretium. Suspendisse vitae sodales tortor, id scelerisque mi. Sed venenatis eros eu risus eleifend, eget facilisis arcu vestibulum. Sed eu lectus ut libero pulvinar viverra a at turpis. Praesent in metus lacus. Nulla molestie odio ut tortor sagittis accumsan. Vestibulum convallis, lacus consequat condimentum facilisis, purus tellus eleifend neque, nec semper ipsum libero a mi. Morbi sed sem at est feugiat ullamcorper et iaculis nibh. Aenean vel ligula eget magna laoreet vestibulum. Integer vitae nibh iaculis, facilisis nulla ut, pretium neque. Vivamus ullamcorper turpis id metus pulvinar, in consectetur enim finibus.</p>\r\n<p>Pellentesque sollicitudin tellus leo. Sed in elementum justo. Donec et orci lorem. Ut dapibus tempor maximus. Cras ipsum ipsum, commodo at ante eu, pellentesque efficitur velit. Quisque massa diam, tincidunt vitae nunc at, ultricies efficitur augue. Vestibulum eu justo fermentum, tempus lorem nec, ultricies diam. Aliquam fringilla, est non interdum bibendum, ipsum massa ultrices magna, eget posuere magna nulla pellentesque risus.</p>\r\n<p>Duis sed est pretium, efficitur lacus at, malesuada enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur cursus id ipsum ut lacinia. Maecenas finibus vehicula enim sed ullamcorper. Cras malesuada nibh eget aliquam aliquam. Curabitur accumsan rutrum dictum. Sed ac leo accumsan, rhoncus velit ac, finibus nibh. Nullam cursus dolor tortor, non mollis urna aliquam sed. Cras rhoncus nisl vitae mauris fermentum, eget suscipit velit convallis. Vestibulum lectus dui, consectetur non pharetra vel, egestas eget ex.</p>\r\n<p>Maecenas aliquam in velit id euismod. Pellentesque id ex neque. Fusce suscipit sed ante at dignissim. Curabitur odio nulla, pretium vitae magna a, scelerisque bibendum justo. Praesent quis lectus facilisis, dapibus mi id, euismod metus. Proin id faucibus urna. Vestibulum ut auctor neque. Praesent ut vehicula ligula. Maecenas nec volutpat ipsum, in auctor odio. Fusce tincidunt interdum odio sed egestas. Curabitur non libero ullamcorper, placerat risus vitae, faucibus neque. Aenean justo nisl, viverra quis porttitor eget, porttitor et augue. Maecenas rutrum augue nec neque lacinia dictum. Vivamus nulla ante, posuere ut malesuada non, facilisis at orci. Nulla imperdiet accumsan orci, a semper massa condimentum vel. Donec convallis metus nisi, sed elementum quam dictum quis.</p>\r\n<p>Sed ullamcorper lectus lorem, consequat sagittis odio mollis non. Aenean pretium rhoncus sapien non elementum. Nam gravida est finibus diam lacinia, et bibendum velit sollicitudin. Fusce fermentum elementum ex, sed ultricies orci fringilla non. Quisque euismod pellentesque urna a scelerisque. Vivamus odio neque, auctor vel viverra eu, scelerisque sed nisi. Sed posuere sodales mauris sit amet rhoncus. Sed quis tortor id dolor ultrices ultrices ac non elit.</p>\r\n<p>Suspendisse aliquam erat ac justo dignissim, nec laoreet purus mollis. Curabitur eget lacus sed augue pellentesque vulputate id sed orci. Nunc vel maximus purus. Curabitur viverra faucibus nunc nec placerat. Praesent quis magna pulvinar, ornare erat ut, porta magna. Curabitur ut leo urna. Nam ultrices porta neque, a venenatis massa laoreet quis. Vestibulum pellentesque sit amet tellus sed vestibulum. Nam quis varius justo. Nullam at lacinia dolor, sed rutrum purus. Vivamus ultricies nisl in convallis fringilla.</p>\r\n<p>Mauris sollicitudin a tortor ut pharetra. Maecenas ornare arcu vestibulum, sagittis odio id, gravida magna. Ut sagittis venenatis neque eu tincidunt. Integer cursus molestie dolor, quis gravida velit. Pellentesque vel ipsum ac sapien accumsan placerat vitae quis tellus. Proin magna dolor, sodales non velit ac, congue tristique quam. Nulla tristique diam et purus eleifend, quis posuere sem mollis. Nullam non dolor ac ante imperdiet tempor. Praesent ac congue lacus. Phasellus dignissim vel dolor rutrum varius. Fusce sagittis quis augue vitae sagittis.</p>\r\n<p>Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur eros erat, accumsan et viverra nec, porttitor quis turpis. Quisque vitae orci magna. Vivamus auctor vestibulum lectus, ut convallis mi faucibus quis. Phasellus leo arcu, porttitor a nunc in, bibendum euismod metus. Donec quis eros nisi. Curabitur mollis, elit hendrerit laoreet fermentum, augue diam varius metus, ac vulputate augue ligula at mi. Mauris placerat, lectus vel ornare auctor, lectus tortor eleifend justo, a suscipit nunc velit luctus nibh. Quisque finibus lacinia tellus luctus faucibus. Nunc vel est eu mi lacinia aliquam vitae nec enim. Sed sodales commodo sapien auctor condimentum. Sed sollicitudin euismod est, at pretium nibh mollis quis. Pellentesque sodales congue mauris, euismod varius enim porttitor a.</p>', '2017-05-30 16:16:31', '2017-05-30 16:16:31'),
(57, 'Jean Forteroche', '909088.jpg', 'Cras rhoncus mattis est, tempus feugiat ante suscipit iaculis', '<p>Sed pharetra leo justo, non feugiat magna pulvinar quis. Vivamus tempor ac enim ac facilisis. Nulla eu odio elit. Aliquam elementum tortor velit, id euismod felis fermentum nec. Nam blandit nunc ut ligula dapibus, et fermentum odio volutpat. Pellentesque sit amet fringilla ligula, nec molestie massa. Nunc auctor maximus erat, nec lacinia eros euismod quis. Vivamus quis elit vitae ante semper porttitor non et nisl. Quisque ac feugiat odio, sed rhoncus sem. In ultrices pretium efficitur. Curabitur eget purus purus. Praesent blandit arcu sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent id blandit elit.</p>\r\n<p>Aliquam erat volutpat. Aliquam eleifend erat ut purus viverra, nec rhoncus tellus consectetur. Phasellus vitae tortor et mi bibendum consectetur. Nunc quis placerat lorem. Nam est felis, venenatis sit amet dictum eu, porttitor et turpis. Morbi elementum commodo semper. Praesent ut finibus neque. Etiam id dapibus arcu. Nunc posuere eget nisi ut posuere. Ut fringilla a diam et semper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam odio risus, ullamcorper vitae sapien at, aliquam consectetur elit.</p>\r\n<p>Ut accumsan blandit sagittis. Mauris ante ex, vestibulum vitae pulvinar aliquam, rhoncus vitae massa. In dictum aliquam nunc ac rhoncus. Curabitur tellus purus, bibendum ut vestibulum nec, maximus ac augue. Maecenas commodo, mauris ac suscipit consequat, tellus lorem varius ligula, at semper turpis dui in lorem. Sed lobortis, ligula eu faucibus commodo, leo lacus viverra nisl, ut suscipit nibh ligula eget lorem. In faucibus ultricies eros, vitae eleifend felis dictum in. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus eget odio eu erat venenatis tempor. Proin eget sem felis.</p>\r\n<p>Cras rhoncus mattis est, tempus feugiat ante suscipit iaculis. Quisque nec dui mattis, placerat nibh eu, venenatis urna. Nullam facilisis tortor in lacus aliquet, non ultricies dui pretium. Suspendisse vitae sodales tortor, id scelerisque mi. Sed venenatis eros eu risus eleifend, eget facilisis arcu vestibulum. Sed eu lectus ut libero pulvinar viverra a at turpis. Praesent in metus lacus. Nulla molestie odio ut tortor sagittis accumsan. Vestibulum convallis, lacus consequat condimentum facilisis, purus tellus eleifend neque, nec semper ipsum libero a mi. Morbi sed sem at est feugiat ullamcorper et iaculis nibh. Aenean vel ligula eget magna laoreet vestibulum. Integer vitae nibh iaculis, facilisis nulla ut, pretium neque. Vivamus ullamcorper turpis id metus pulvinar, in consectetur enim finibus.</p>\r\n<p>Pellentesque sollicitudin tellus leo. Sed in elementum justo. Donec et orci lorem. Ut dapibus tempor maximus. Cras ipsum ipsum, commodo at ante eu, pellentesque efficitur velit. Quisque massa diam, tincidunt vitae nunc at, ultricies efficitur augue. Vestibulum eu justo fermentum, tempus lorem nec, ultricies diam. Aliquam fringilla, est non interdum bibendum, ipsum massa ultrices magna, eget posuere magna nulla pellentesque risus.</p>\r\n<p>Duis sed est pretium, efficitur lacus at, malesuada enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur cursus id ipsum ut lacinia. Maecenas finibus vehicula enim sed ullamcorper. Cras malesuada nibh eget aliquam aliquam. Curabitur accumsan rutrum dictum. Sed ac leo accumsan, rhoncus velit ac, finibus nibh. Nullam cursus dolor tortor, non mollis urna aliquam sed. Cras rhoncus nisl vitae mauris fermentum, eget suscipit velit convallis. Vestibulum lectus dui, consectetur non pharetra vel, egestas eget ex.</p>', '2017-05-30 16:16:05', '2017-05-30 16:16:05'),
(56, 'Jean Forteroche', '344559.jpg', 'Interdum et malesuada fames ac ante ipsum primis in faucibus', '<p>Aliquam consectetur lorem non ex elementum, at hendrerit tellus scelerisque. Nulla felis dolor, finibus eu odio sit amet, tempor mollis velit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dapibus efficitur porta. Cras vulputate dapibus justo, in tincidunt sem posuere vel. Nullam justo orci, venenatis ut massa a, lacinia laoreet orci. Aenean hendrerit lacinia pretium. Nam scelerisque metus nunc, nec tempor nulla lacinia ut. Pellentesque sodales eleifend viverra. Nullam nibh lacus, laoreet id orci vel, venenatis sagittis massa.</p>\r\n<p>Donec quis laoreet leo, quis mollis velit. Nulla tellus justo, fringilla id blandit ut, fermentum a augue. Nullam porttitor tristique turpis, eget sollicitudin nulla rhoncus eu. Suspendisse eu rutrum risus, et suscipit nulla. Nunc sollicitudin lobortis fermentum. Donec ut maximus enim, ut consectetur nibh. Nam facilisis lacinia euismod. Integer consectetur laoreet fermentum.</p>\r\n<p>Sed ullamcorper lectus lorem, consequat sagittis odio mollis non. Aenean pretium rhoncus sapien non elementum. Nam gravida est finibus diam lacinia, et bibendum velit sollicitudin. Fusce fermentum elementum ex, sed ultricies orci fringilla non. Quisque euismod pellentesque urna a scelerisque. Vivamus odio neque, auctor vel viverra eu, scelerisque sed nisi. Sed posuere sodales mauris sit amet rhoncus. Sed quis tortor id dolor ultrices ultrices ac non elit.</p>\r\n<p>Suspendisse aliquam erat ac justo dignissim, nec laoreet purus mollis. Curabitur eget lacus sed augue pellentesque vulputate id sed orci. Nunc vel maximus purus. Curabitur viverra faucibus nunc nec placerat. Praesent quis magna pulvinar, ornare erat ut, porta magna. Curabitur ut leo urna. Nam ultrices porta neque, a venenatis massa laoreet quis. Vestibulum pellentesque sit amet tellus sed vestibulum. Nam quis varius justo. Nullam at lacinia dolor, sed rutrum purus. Vivamus ultricies nisl in convallis fringilla.</p>\r\n<p>Mauris sollicitudin a tortor ut pharetra. Maecenas ornare arcu vestibulum, sagittis odio id, gravida magna. Ut sagittis venenatis neque eu tincidunt. Integer cursus molestie dolor, quis gravida velit. Pellentesque vel ipsum ac sapien accumsan placerat vitae quis tellus. Proin magna dolor, sodales non velit ac, congue tristique quam. Nulla tristique diam et purus eleifend, quis posuere sem mollis. Nullam non dolor ac ante imperdiet tempor. Praesent ac congue lacus. Phasellus dignissim vel dolor rutrum varius. Fusce sagittis quis augue vitae sagittis.</p>\r\n<p>Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur eros erat, accumsan et viverra nec, porttitor quis turpis. Quisque vitae orci magna. Vivamus auctor vestibulum lectus, ut convallis mi faucibus quis. Phasellus leo arcu, porttitor a nunc in, bibendum euismod metus. Donec quis eros nisi. Curabitur mollis, elit hendrerit laoreet fermentum, augue diam varius metus, ac vulputate augue ligula at mi. Mauris placerat, lectus vel ornare auctor, lectus tortor eleifend justo, a suscipit nunc velit luctus nibh. Quisque finibus lacinia tellus luctus faucibus. Nunc vel est eu mi lacinia aliquam vitae nec enim. Sed sodales commodo sapien auctor condimentum. Sed sollicitudin euismod est, at pretium nibh mollis quis. Pellentesque sodales congue mauris, euismod varius enim porttitor a.</p>\r\n<p>Sed pharetra leo justo, non feugiat magna pulvinar quis. Vivamus tempor ac enim ac facilisis. Nulla eu odio elit. Aliquam elementum tortor velit, id euismod felis fermentum nec. Nam blandit nunc ut ligula dapibus, et fermentum odio volutpat. Pellentesque sit amet fringilla ligula, nec molestie massa. Nunc auctor maximus erat, nec lacinia eros euismod quis. Vivamus quis elit vitae ante semper porttitor non et nisl. Quisque ac feugiat odio, sed rhoncus sem. In ultrices pretium efficitur. Curabitur eget purus purus. Praesent blandit arcu sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent id blandit elit.</p>', '2017-05-30 16:15:37', '2017-05-30 16:15:37'),
(55, 'Jean Forteroche', '301816.jpg', 'Sed semper ex quis tristique hendrerit', '<p>Nam in convallis leo, non gravida nisi. Sed et risus turpis. Donec accumsan tincidunt risus eget sodales. In eu odio sed orci fringilla dictum. Phasellus ac sapien et erat maximus efficitur. Proin ullamcorper ligula ac gravida faucibus. Mauris vitae ligula nec ipsum vestibulum egestas quis efficitur lectus. Donec interdum accumsan nunc, in viverra enim tempor ac. Fusce sollicitudin accumsan sem a bibendum.</p>\r\n<p>Duis id rhoncus felis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eu fringilla ante. Phasellus eget ligula tristique, cursus odio ac, maximus nulla. Proin non libero justo. Vivamus aliquet, lorem ac efficitur fermentum, mi tellus posuere massa, sit amet ultrices diam felis sed mauris. Pellentesque mi turpis, sodales vel nunc sed, congue vehicula nibh. Aliquam ligula nulla, ullamcorper a ligula vitae, ullamcorper scelerisque lacus.</p>\r\n<p>Nam bibendum placerat sapien, pulvinar facilisis odio auctor a. In ut tincidunt urna. Vivamus mattis augue id velit imperdiet, vitae lobortis massa commodo. Mauris dignissim congue urna fermentum molestie. Etiam at pulvinar sapien. Suspendisse velit arcu, placerat in quam et, ullamcorper rutrum lorem. Pellentesque quis nibh at justo gravida suscipit id viverra nisl. Mauris volutpat imperdiet tellus, vel imperdiet sem varius ut. Quisque id semper risus, ut lacinia mauris.</p>\r\n<p>In semper placerat lacus a feugiat. Aliquam dictum elit urna, et feugiat metus ornare ut. Suspendisse potenti. Mauris ex nunc, fringilla sit amet dolor eu, gravida ullamcorper metus. Donec sollicitudin vulputate dolor, in scelerisque felis aliquet pulvinar. Proin id ante nec nisl placerat tincidunt ac vitae sapien. Pellentesque euismod nibh tortor, congue fringilla lorem auctor quis. Quisque sit amet massa at orci sodales ullamcorper. Ut dictum imperdiet diam a egestas. Suspendisse id eleifend eros.</p>\r\n<p>Sed semper ex quis tristique hendrerit. Integer mollis ac orci ut pharetra. Mauris in tellus porttitor, pharetra nisi vel, gravida velit. Ut lacinia ultrices tincidunt. Quisque vitae tempor augue, quis varius dolor. Donec in pharetra metus. Donec fermentum tellus sed lacus porta, at varius orci vestibulum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas tincidunt viverra metus, in vulputate leo lacinia sed. Phasellus finibus in turpis sit amet ullamcorper. Phasellus vitae euismod nunc. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum in ante sollicitudin, porta felis vel, feugiat augue. Quisque eget tempor purus. Etiam nisl justo, convallis sit amet sem sit amet, varius viverra justo. Sed ultrices eros dolor, non faucibus massa dignissim eu.</p>\r\n<p>Aliquam consectetur lorem non ex elementum, at hendrerit tellus scelerisque. Nulla felis dolor, finibus eu odio sit amet, tempor mollis velit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dapibus efficitur porta. Cras vulputate dapibus justo, in tincidunt sem posuere vel. Nullam justo orci, venenatis ut massa a, lacinia laoreet orci. Aenean hendrerit lacinia pretium. Nam scelerisque metus nunc, nec tempor nulla lacinia ut. Pellentesque sodales eleifend viverra. Nullam nibh lacus, laoreet id orci vel, venenatis sagittis massa.</p>\r\n<p>Donec quis laoreet leo, quis mollis velit. Nulla tellus justo, fringilla id blandit ut, fermentum a augue. Nullam porttitor tristique turpis, eget sollicitudin nulla rhoncus eu. Suspendisse eu rutrum risus, et suscipit nulla. Nunc sollicitudin lobortis fermentum. Donec ut maximus enim, ut consectetur nibh. Nam facilisis lacinia euismod. Integer consectetur laoreet fermentum.</p>', '2017-05-30 16:14:07', '2017-05-30 16:14:07'),
(59, 'Jean Forteroche', '791316.jpg', 'Aliquam consectetur lorem non ex elementum', '<p>Aliquam consectetur lorem non ex elementum, at hendrerit tellus scelerisque. Nulla felis dolor, finibus eu odio sit amet, tempor mollis velit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dapibus efficitur porta. Cras vulputate dapibus justo, in tincidunt sem posuere vel. Nullam justo orci, venenatis ut massa a, lacinia laoreet orci. Aenean hendrerit lacinia pretium. Nam scelerisque metus nunc, nec tempor nulla lacinia ut. Pellentesque sodales eleifend viverra. Nullam nibh lacus, laoreet id orci vel, venenatis sagittis massa.</p>\r\n<p>&nbsp;</p>\r\n<p>Aliquam consectetur lorem non ex elementum, at hendrerit tellus scelerisque. Nulla felis dolor, finibus eu odio sit amet, tempor mollis velit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dapibus efficitur porta. Cras vulputate dapibus justo, in tincidunt sem posuere vel. Nullam justo orci, venenatis ut massa a, lacinia laoreet orci. Aenean hendrerit lacinia pretium. Nam scelerisque metus nunc, nec tempor nulla lacinia ut. Pellentesque sodales eleifend viverra.</p>\r\n<p>&nbsp;</p>\r\n<p>Aliquam consectetur lorem non ex elementum, at hendrerit tellus scelerisque. Nulla felis dolor, finibus eu odio sit ametAliquam consectetur lorem non ex elementum, at hendrerit tellus scelerisque. Nulla felis dolor, finibus eu odio sit amet.</p>\r\n<p>&nbsp;</p>', '2017-05-30 17:48:00', '2017-05-30 17:48:00');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `image`) VALUES
(18, 'admin', 'a2990cf3f273a589a48f031db824c3d2ffe284ab', '747750.png'),
(19, 'Jean Forteroche', 'a2990cf3f273a589a48f031db824c3d2ffe284ab', '107796.jpg');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`commentId`);

--
-- Index pour la table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `comments`
--
ALTER TABLE `comments`
  MODIFY `commentId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT pour la table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
