<?php
session_start();

define("DS", DIRECTORY_SEPARATOR);
define("ROOT", dirname(dirname(__FILE__)));

define("WEB", ROOT.DS."web");
define("APP", ROOT.DS."app");

define("LIB", APP.DS."lib");
define("CONF", APP.DS."config");
define("MVC", APP.DS."mvc");
define("TEMP", APP.DS."templates");

define("NEWS_IMG", WEB.DS."img".DS."news");
define("USERS_IMG", WEB.DS."img".DS."users");

define("ADMIN_VIEW", MVC.DS."views".DS."admin");
define("HOME_VIEW", MVC.DS."views".DS."home");

require_once(LIB.DS."init.php");
require_once(CONF.DS."config.php");
require_once(LIB . DS . "dbPDO.php");
require_once(LIB.DS."SplClassLoader.php");

$URI = $_SERVER['REQUEST_URI'];
$URI = str_replace("app_mvc/", "", $URI);
$URI = urldecode(trim($URI, "/"));

$app = App::run($URI);
