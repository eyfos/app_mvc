<?php
/**
 * Created by PhpStorm.
 * User: Saiya
 * Date: 5/4/2017
 * Time: 01:12 AM
 */

trait Hydrator {
    public function hydrate($data)
    {
        foreach ($data as $key => $value)
        {
            $method = 'set'.ucfirst($key);

            if (is_callable([$this, $method]))
            {
                $this->$method($value);
            }
        }
    }
}