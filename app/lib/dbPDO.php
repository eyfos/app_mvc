<?php
/**
 * Created by PhpStorm.
 * User: Saiya
 * Date: 5/4/2017
 * Time: 12:26 AM
 */

class Db {

    //Instance du manager
    private static  $_managerInstance;

    // Instance PDO
    private $instancePDO;

    /**
     * Db constructor.
     *
     * Cette fonction te construit ta connexion a la bdd
     *
     * @throws Exception
     */
    public function __construct() {
        $this->instancePDO = new PDO("mysql:host=localhost;dbname=app_mvc", 'root', '');
        $this->instancePDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        if(!$this->instancePDO){
            try{
                $this->instancePDO = new PDO("mysql:host=localhost;dbname=app_mvc", 'root', '');
                $this->instancePDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }catch(PDOException $e){
                throw new Exception('Erreur');
            }
        }

        return $this;
    }

    /**
     * Ferme la connexion à la bdd.
     */
    public function __destruct() {

        self::$_managerInstance = null;
    }

    /**
     * Recupère l'instance de connexion
     *
     * @return Db
     */
    public static function getInstance(){
        if (!self::$_managerInstance){
            self::$_managerInstance = new Db();
        }
        return self::$_managerInstance;
    }

    /**
     * Recupere l'instance PDO
     * @return PDO
     */
    public function getPDOInstance(){
        return $this->instancePDO;
    }

}