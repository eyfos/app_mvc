<?php
/**
 * Created by PhpStorm.
 * User: Saiya
 * Date: 5/2/2017
 * Time: 03:27 PM
 */

class Config {

    protected static $setting = array();

    /**
     * @return array
     */
    public static function get($key)
    {
        return isset(self::$setting[$key]) ? self::$setting[$key] : null;
    }

    public static function set($key, $value) {
        self::$setting[$key] = $value;
    }
}