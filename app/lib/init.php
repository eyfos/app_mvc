<?php
/**
 * Created by PhpStorm.
 * User: Saiya
 * Date: 5/2/2017
 * Time: 03:30 PM
 */

    function __autoload($className) {
        $libPath = LIB.DS.strtolower($className)."Class.php";
        $controllerPath = MVC.DS."controllers".DS.str_replace("controller", "", strtolower($className))."Controller.php";

        if (file_exists($libPath)) {
            require_once($libPath);
        } elseif (file_exists($controllerPath)) {
            require_once($controllerPath);
        }
        else {
            require_once(TEMP.DS."errors".DS."404.php");
        }

    }