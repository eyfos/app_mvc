<?php
/**
 * Created by PhpStorm.
 * User: Saiya
 * Date: 5/3/2017
 * Time: 02:37 PM
 */

class View {

    protected $data;
    protected $path;

    public static function getDefaultViewPath() {
        $router = App::getRouter();
        if (!$router) {
            return false;
        }
        $controllerDir = $router->getController();
        $templateName = $router->getAction().".php";
        return MVC.DS."views".DS.$controllerDir.DS.$templateName;
    }

    function __construct($data = array(), $path = null) {
        if (!$path) {
            $path = self::getDefaultViewPath();
        }
        if (!file_exists($path)) {
            throw new Exception("Le template n'existe pas", 1);
        }
        $this->data = $data;
        $this->path = $path;
    }

    public function render() {
        $data = $this->data;

        ob_start();
        include $this->path;
        $content = ob_get_clean();
        return $content;
    }
}