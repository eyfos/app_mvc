<?php
/**
 * Created by PhpStorm.
 * User: Saiya
 * Date: 5/2/2017
 * Time: 04:29 PM
 */
/* error_reporting(0);
ini_set('display_errors', 0); */
class App {

    protected static $router;
    protected static $db;

    public static function getRouter() {
        return self::$router;
    }

    public static function run($URI) {
        self::$router = new Router($URI);

        $class = str_replace(" ", "", self::$router->getController());

        $controllerClass = ucfirst($class . "Controller");
        $controllerMethod = strtolower(self::$router->getAction());

        $controllerObject = new $controllerClass();

        if(method_exists($controllerObject, $controllerMethod)) {

          $controllerObject->$controllerMethod();
          /**$viewPath = $controllerObject->$controllerMethod();
          $viewObj = new View($controllerObject->getData(), $viewPath);
          $content = $viewObj->render();**/
        } else {
            require_once(TEMP.DS."errors".DS."404.php");
        }
       /** $layout = Config::get("default_layout").".html";
        $layoutPath = APP.DS."templates".$layout;
        $layoutViewObj = new View(compact("content").$layoutPath);
        echo $layoutViewObj->render(); **/
    }
}