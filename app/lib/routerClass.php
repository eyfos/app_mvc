<?php
/**
 * Created by PhpStorm.
 * User: Saiya
 * Date: 5/2/2017
 * Time: 03:53 PM
 */

class Router {

    protected $URI;
    protected $controller;
    protected $action;
    protected $params;

    /**
     * @return mixed
     */
    public function getURI() {
        return $this->URI;
    }

    /**
     * @param mixed $URI
     */
    public function setURI($URI) {
        $this->URI = $URI;
    }

    /**
     * @return mixed
     */
    public function getController() {
        return $this->controller;
    }

    /**
     * @param mixed $controller
     */
    public function setController($controller) {
        $this->controller = $controller;
    }

    /**
     * @return mixed
     */
    public function getAction() {
        return $this->action;
    }

    /**
     * @param mixed $action
     */
    public function setAction($action) {
        $this->action = $action;
    }

    /**
     * @return mixed
     */
    public function getParams() {
        return $this->params;
    }

    /**
     * @param mixed $params
     */
    public function setParams($params) {
        $this->params = $params;
    }

    function __construct($URI) {

        // Set controllers and actions
        $this->controller = Config::get("default_controller");
        $this->action = Config::get("default_action");

        $URIParts = explode("?", $URI);
        $path = $URIParts[0];
        $pathParts = explode("/", $path);


        if(count($pathParts)) {
            if(current($pathParts)) {
                $this->controller = strtolower(current($pathParts));
                array_shift($pathParts);
            }

            if(current($pathParts)) {
                $this->action = strtolower(current($pathParts));
                array_shift($pathParts);
            }
            $this->params = $pathParts;
        }

    }
}