<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?= isset($pageTitle) ? $pageTitle . " - " . Config::get("siteName") : Config::get("siteName"); ?></title>
    
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="/app_mvc/web/js/aos.js"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/app_mvc/web/css/style.css" />
    <link rel="stylesheet" type="text/css" href="/app_mvc/web/css/aos.css" />
    <link rel="stylesheet" type="text/css" href="/app_mvc/web/css/font-awesome.min.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Signika" rel="stylesheet">

    <script>tinymce.init({ selector:'textarea'});</script>

    <script>
        $(document).ready(function(){
            var offset = $(".navBarmenu").offset().top;
            $(document).scroll(function(){
                var scrollTop = $(document).scrollTop();
                if(scrollTop > offset){
                    $(".navBarmenu").css("position", "fixed");
                    $(".navBarmenu").css("top", "0");
                    $(".navBarmenu").css("padding", "0");
                    $(".navBarmenu").css("font-size", "11pt");
                }
                else {
                    $(".navBarmenu").css("position", "static");
                    $(".navBarmenu").css("font-size", "13pt");
                    $(".navBarmenu").css("padding", "15px 0 15px 0");
                }
            });
        });
    </script>
    
    <script>
        $(document).ready(function(){
            $(window).bind('scroll',function(e){
                parallaxScroll();
            });

            function parallaxScroll(){
                var scrolledY = $(window).scrollTop();
                $('.headerInfos').css('margin-top','-'+((scrolledY*0.5))+'px');
            }
        });
    </script>

    <script>
        $(function() {
            AOS.init({duration: 1200});
        });
    </script>

    <script>
        $(document).on('click', 'a', function(event){
            $('html, body').animate({
                scrollTop: $( $.attr(this, 'href') ).offset().top
            }, 500);
        });
    </script>
</head>
<body data-spy="scroll" data-target=".navBarmenu" data-offset="50">