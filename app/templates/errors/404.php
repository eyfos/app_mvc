<?php
$pageTitle = "Page non trouvée...";
require_once(TEMP . DS . "default.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>404 page not found</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center error">
                <img src="/app_mvc/web/img/404.jpg" />
                <span class="errorNotFound">
                    Erreur 404
                </span>
                <span class="errorInfos">
                    La page que vous cherchez n'existe pas ou l'action demandée n'est pas autorisée.
                </span>
                <span class="retour">
                    <a href="/app_mvc/">Retour au site <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                </span>
            </div>
        </div>
    </div>
</body>
</html>