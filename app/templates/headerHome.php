<?php require_once(TEMP . DS . "default.php"); ?>

<header id="header" class="header">
    <div class="login">
        <?php if (isset($_SESSION['authenticated']) && $_SESSION['authenticated'] == true) { ?>
            <a href="<?php if (isset($_SESSION['authenticated']) && $_SESSION['authenticated'] == true) {
                echo "/app_mvc/admin/index";} else { echo "/app_mvc/admin/login";} ?>">
                <i class="fa fa-cogs" aria-hidden="true"></i>
            </a>
        <?php }
        else { ?>
            <a href="<?php if (isset($_SESSION['authenticated']) && $_SESSION['authenticated'] == true) {
                echo "/app_mvc/admin/index";} else { echo "/app_mvc/admin/login";} ?>">
                <i class="fa fa-user" aria-hidden="true"></i><i class="fa fa-sign-in" aria-hidden="true"></i>
            </a>
            <?php
        }
        ?>
    </div>
    <div class="headerInfos">
        <div class="infoImage"><img src="/app_mvc/web/img/5_thumb.jpg" /></div>
        <div class="infoName">Jean Forteroche</div>
        <div class="infoDesc">acteur & écrivain</div>
        <div class="infoSocial">
            <span class="socialFb"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></span>
            <span class="socialTwitter"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></span>
            <span class="socialLinkedIn"><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></span>
            <span class="socialPinterest"><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></span>
            <span class="socialEmail"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></span>
        </div>
    </div>
</header>

<div class="mainContainer">
