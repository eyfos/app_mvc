<?php
require_once(TEMP . DS . "default.php");
$commentManager = new CommentManager();
$reportedComments = $commentManager->selectReported();
$countReports = $commentManager->countReported();
$commentManager->removeReport();
?>

<nav class="navbar navbar-inverse top-bar navbar-fixed-top">
    <div class="container-fluid">
        <div class="adminHome"><a href="/app_mvc"> <i class="fa fa-home" aria-hidden="true"></i>Accueil</a></div>
        <ul class="nav navbar-nav pull-right">
            <li class="dropdown reportedComments">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true">
                    <?php
                    if (!empty($reportedComments)) { ?>
                        <i class="fa fa-exclamation-circle fa-red" aria-hidden="true"></i> Signalements <?php
                    } else { ?>
                        <i class="fa fa-exclamation-circle" aria-hidden="true"></i> Signalements <?php
                    }
                    ?>
                </a>
                <ul class="dropdown-menu reports">
                    <div class="reportsHeader">
                        Commentaires signalés
                    </div>

                    <?php
                    if (!empty($reportedComments)) {
                        foreach ($reportedComments as $reported) { ?>
                            <li class="reported">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="reportedImg">
                                            <img src="/app_mvc/web/img/default_user.png" class="img-fluid" />
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="reportedAuthor">
                                            <div><?php echo $reported['author'] ?></div>
                                        </div>
                                        <div class="reportedContent">
                                            <span>
                                                <?php $reported['content'] = strip_tags($reported['content']);
                                                if (strlen($reported['content']) > 100) {
                                                    $stringCut = substr($reported['content'], 0, 100);
                                                    $reported['content'] = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
                                                } ?>
                                                <?php echo $reported['content']; ?>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <span class="verifReport">
                                            <a href="/app_mvc/admin/removeReport?commentId=<?php echo htmlspecialchars($reported['commentId']); ?>">
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                            </a>
                                        </span>
                                        <span class="deleteIcon">
                                            <a href="/app_mvc/admin/deleteComment?commentId=<?php echo htmlspecialchars($reported['commentId']); ?>">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </a>
                                        </span>
                                    </div>
                            </li>
                            <?php
                        }
                    } else { ?>
                        <div class="noData">
                            Aucun commentaire signalé
                        </div>
                    <?php } ?>
                    <div class="reportsFooter">
                        <a href="/app_mvc/admin/showReported">
                            Voir tous les signalements <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                    </div>
                </ul>
            </li>

            <li class="dropdown userSettings">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true">
                    <?php echo $_SESSION['username'] ?>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li> <a href="/app_mvc/admin/showUsers"><i class="fa fa-cog"></i> Configuration</a> </li>
                    <li> <a href="/app_mvc/admin/logout"><i class="fa fa-power-off"></i> Déconnexion</a> </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>

<div class="wrapper" id="wrapper">
    <div class="sidebarLeft">
        <div class="connectedInfo">
            <span><i class="fa fa-key" aria-hidden="true"></i><em>Connecté en tant que</em></span>
                <img src="/app_mvc/web/img/users/<?php echo $_SESSION['image'] ?>" class="img-fluid" />
            <span><?php echo $_SESSION['username'];?></span>
        </div>

        <ul id="sidebarNav">
            <li class="sidebarPanel">
                <a href="/app_mvc/admin" class="sidebarElement"><i class="fa fa-tachometer" aria-hidden="true"></i> Tableau de bord</a>
            </li>
            <li class="sidebarPanel">
                <a href="javascript:;" data-toggle="collapse" data-target="#navNews" class="collapsed sidebarElement" aria-expanded="false">
                    <i class="fa fa-pencil"></i> News <i class="fa fa-caret-down pull-right"></i>
                </a>

                <ul class="nav collapse" id="navNews" aria-expanded="false" style="height: 0px;">
                    <li>
                        <a href="/app_mvc/admin/showNews"><i class="fa fa-angle-double-right"></i> Voir les news</a>
                    </li>
                    <li>
                        <a href="/app_mvc/admin/addNews"><i class="fa fa-angle-double-right"></i> Ajouter une news</a>
                    </li>
                </ul>
            </li>
            <li class="sidebarPanel">
                <a href="javascript:;" data-toggle="collapse" data-target="#comments" class="collapsed sidebarElement" aria-expanded="false">
                    <i class="fa fa-comments"></i> Commentaires <i class="fa fa-caret-down pull-right"></i>
                </a>

                <ul class="nav collapse" id="comments" aria-expanded="false" style="height: 0px;">
                    <li>
                        <a href="/app_mvc/admin/showComments"><i class="fa fa-angle-double-right"></i> Gestion des commentaires</a>
                        <a href="/app_mvc/admin/showReported"><i class="fa fa-angle-double-right"></i> Commentaires signalés</a>

                    </li>
                </ul>
            </li>
            <li class="sidebarPanel">
                <a href="javascript:;" data-toggle="collapse" data-target="#users" class="collapsed sidebarElement" aria-expanded="false">
                    <i class="fa fa-users"></i> Utilisateurs <i class="fa fa-caret-down pull-right"></i>
                </a>

                <ul class="nav collapse" id="users" aria-expanded="false" style="height: 0px;">
                    <li>
                        <a href="/app_mvc/admin/showUsers"><i class="fa fa-angle-double-right"></i> Voir les utilisateurs</a>
                    </li>
                    <li>
                        <a href="/app_mvc/admin/addUser"><i class="fa fa-angle-double-right"></i> Ajouter un utilisateur</a>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="adminCopyright">
            <?php echo Config::get("siteName"); ?> <br> © <?php echo date("Y");?> Copyright
        </div>
    </div>
</div>

<div class="container-fluid adminContainer">
    <div class="viewZone">