<?php
/**
 * Created by PhpStorm.
 * User: Saiya
 * Date: 7/13/2017
 * Time: 12:27 PM
 */
?>
<div class="navBarmenu">
    <nav class="navbar navbar-default navbar-custom">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li><a href="#news"><i class="fa fa-home" aria-hidden="true"></i> Accueil</a></li>
                        <li><a href="#about">à propos</a></li>
                        <li><a href="#bio">Biographie</a></li>
                        <li><a href="#presse">Presse</a></li>
                        <li><a href="#contact">Contact</a></li>
                    </ul>

                    <ul class="nav navbar-nav pull-right">
                        <li><a href="/app_mvc/home/chapitres"><i class="fa fa-book" aria-hidden="true"></i> Lire les chapitres</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</div>
