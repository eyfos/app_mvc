<?php
/**
 * Created by PhpStorm.
 * User: Saiya
 * Date: 5/2/2017
 * Time: 03:59 PM
 */
    Config::set("siteName", "Billet simple pour l'Alaska");

    Config::set("default_controller", "home");
    Config::set("default_action", "index");
    Config::set("default_layout", "default");
    Config::set("default_error", "404");