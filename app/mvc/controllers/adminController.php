<?php
/**
 * Created by PhpStorm.
 * User: Saiya
 * Date: 5/5/2017
 * Time: 02:40 PM
 */

require_once(MVC.DS."entity".DS."News.php");
require_once(MVC.DS."entity".DS."User.php");
require_once(MVC.DS."entity".DS."Comment.php");
require_once(MVC.DS."managers".DS."NewsManager.php");
require_once(MVC.DS."managers".DS."CommentManager.php");
require_once(MVC.DS."managers".DS."UserManager.php");

class AdminController extends Controller {

    function __construct($data = array()) {
        parent::__construct($data);
    }

    public function index() {
        if (!isset($_SESSION['authenticated'])) {
            require_once(ADMIN_VIEW.DS."modules".DS."users".DS."login.php");
            die();
        }

        $newsManager = new NewsManager();
        $commentManager = new CommentManager();
        $userManager = new UserManager();

        $dataNews = $newsManager->count();
        $dataUsers = $userManager->count();
        $dataComments = $commentManager->count();


        $data = ['users' => $dataUsers, 'news' =>  $dataNews, 'Comment' =>  $dataComments];

        $userImage = $userManager->getUserImage($_SESSION['username']);

        $pageTitle = "Administration";

        require_once(ADMIN_VIEW.DS."index.php");
    }

    //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////// NEWS ////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////

    public function addNews() {

        if (!isset($_SESSION['authenticated'])) {
            require_once(ADMIN_VIEW.DS."modules".DS."users".DS."login.php");
            die();
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            if (!isset($_POST['title']) || empty($_POST['title']) ||
                !isset($_POST['content']) || empty($_POST['content']) ||
                !isset($_FILES['image']) || empty($_FILES['image'])) {
                ?><script>alert('Veuillez remplir tous les champs du formulaire');</script><?php
            } else {
                $news = new News();

                $news->setAuthor($_SESSION['username']);
                $news->setTitle($_POST['title']);
                $news->setContent($_POST['content']);
                $news->setImage($_FILES['image']);

                $newsManager = new NewsManager();
                $newsId = $newsManager->add($news);

                $news->setId($newsId);

                header('Location: /app_mvc/admin/showNews');
            }
        }

        $pageTitle = "Ajouter une news";

        require_once(ADMIN_VIEW.DS."modules".DS."news".DS."add.php");
    }

    public function editNews() {

        if (!isset($_SESSION['authenticated'])) {
            require_once(ADMIN_VIEW.DS."modules".DS."users".DS."login.php");
            die();
        }

        if (!isset($_GET['id']) || empty($_GET['id']) || !intval($_GET['id'])) {
            require_once(TEMP.DS."errors".DS."404.php");
            die();
        }

        $newsManager = new NewsManager();
        $news = $newsManager->find($_GET['id']);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (!isset($_POST['content']) || !isset($_POST['title']) ||
                empty($_POST['title']) || empty($_POST['content'])) {
                ?><script>alert('Veuillez remplir tous les champs du formulaire');</script><?php
            } else {
                $newsManager->update($news);

                header('Location: /app_mvc/admin/showNews');
            }
        }

        $pageTitle = "Editer une news";

        require_once(ADMIN_VIEW.DS."modules".DS."news".DS."edit.php");
    }

    public function showNews() {

        if (!isset($_SESSION['authenticated'])) {
            require_once(ADMIN_VIEW.DS."modules".DS."users".DS."login.php");
            die();
        }

        $newsManager = new NewsManager();
        $allNews = $newsManager->findAllNewestFirst();

        $pageTitle = "Liste des news";

        require_once(ADMIN_VIEW.DS."modules".DS."news".DS."show.php");
    }

    public function deleteNews() {
        if (!isset($_SESSION['authenticated'])) {
            require_once(ADMIN_VIEW.DS."modules".DS."users".DS."login.php");
            die();
        }

        if (!isset($_GET['id']) || empty($_GET['id']) || !intval($_GET['id'])) {
            require_once(TEMP.DS."errors".DS."404.php");
            die();
        }

        $news = new News();
        $news->setId($_GET['id']);
        $newsManager = new NewsManager();

        $newsManager->find($news);
        $newsManager->delete($news);

        header('Location: /app_mvc/admin/showNews');
    }

    ///////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////// USERS ////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    public function addUser() {

        if (!isset($_SESSION['authenticated'])) {
            require_once(ADMIN_VIEW.DS."modules".DS."users".DS."login.php");
            die();
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (!isset($_POST['username']) || empty($_POST['username']) ||
                !isset($_POST['password']) || empty($_POST['password']) ||
                !isset($_FILES['image']) || empty($_FILES['image'])) {
                ?><script>alert('Veuillez remplir tous les champs du formulaire');</script><?php
            } else {
                $user = new User();

                $user->setUsername($_POST['username']);
                $user->setPassword(sha1('$^";=R6k)aG}9pU' . $_POST['password']));
                $user->setImage($_FILES['image']);

                $userManager = new UserManager();
                $userId = $userManager->add($user);
                $user->setId($userId);


                header('Location: /app_mvc/admin/showUsers');
            }
        }

        $pageTitle = "Ajouter un utilisateur";

        require_once(ADMIN_VIEW.DS."modules".DS."users".DS."add.php");
    }

    public function editUser() {

        if (!isset($_SESSION['authenticated'])) {
            require_once(ADMIN_VIEW.DS."modules".DS."users".DS."login.php");
            die();
        }

        if (!isset($_GET['id']) || empty($_GET['id']) || !intval($_GET['id'])) {
            require_once(TEMP.DS."errors".DS."404.php");
            die();
        }


        $userManager = new UserManager();
        $user = $userManager->find($_GET['id']);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (!isset($_POST['username']) || !isset($_POST['password']) ||
                empty($_POST['username']) || empty($_POST['password'])) {
                ?><script>alert('Veuillez remplir tous les champs du formulaire');</script><?php
            } else {
                $userManager->update($user);

                header('Location: /app_mvc/admin/showUsers');
            }
        }

        $pageTitle = "Modifier un utilisateur";

        require_once(ADMIN_VIEW.DS."modules".DS."users".DS."edit.php");
    }

    public function showUsers() {
        if (!isset($_SESSION['authenticated'])) {
            require_once(ADMIN_VIEW.DS."modules".DS."users".DS."login.php");
            die();
        }

        $userManager = new UserManager();
        $users = $userManager->findAll();

        $pageTitle = "Liste des utilisateurs";

        require_once(ADMIN_VIEW.DS."modules".DS."users".DS."show.php");
    }

    public function deleteUser() {
        if (!isset($_SESSION['authenticated'])) {
            require_once(ADMIN_VIEW.DS."modules".DS."users".DS."login.php");
            die();
        }

        if (!isset($_GET['id']) || empty($_GET['id']) || !intval($_GET['id'])) {
            require_once(TEMP.DS."errors".DS."404.php");
            die();
        }

        $user = new User();
        $userManager = new UserManager();

        $user->setId($_GET['id']);

        $userManager->find($user);
        $userManager->delete($user);

        header('Location: /app_mvc/admin/showUsers');
    }

    public function login() {
        $pageTitle = "Connexion";
        require_once(ADMIN_VIEW.DS."modules".DS."users".DS."login.php");

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $user = new User();
            $userManager = new UserManager();

            $user->setUsername($_POST['username']);
            $user->setPassword($_POST['password']);

            if (!isset($_POST['username']) && !isset($_POST['password'])) {
                ?><script>alert('Veuillez remplir tous les champs');</script><?php
            }

            $userManager->login($user);
            header('Location: /app_mvc/admin');
        }
    }

    public function logout() {
        $userManager = new UserManager();
        $userManager->logout();
    }

    ///////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////// COMMENTS /////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    public function editComment() {
        if (!isset($_SESSION['authenticated'])) {
            require_once(ADMIN_VIEW.DS."modules".DS."users".DS."login.php");
            die();
        }

        if (!isset($_GET['commentId']) || empty($_GET['commentId']) || !intval($_GET['commentId'])) {
            require_once(TEMP.DS."errors".DS."404.php");
            die();
        }

        $commentManager = new CommentManager();
        $comment = $commentManager->find($_GET['commentId']);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (!isset($_POST['content']) || empty($_POST['content'])) {
                ?><script>alert('Veuillez remplir tous les champs');</script><?php
                } else {
                    $commentManager->update($comment);

                    header('Location: /app_mvc/admin/showComments');
                }
        }

        $pageTitle = "Editer un commentaire";

        require_once(ADMIN_VIEW.DS."modules".DS."comments".DS."edit.php");
    }

    public function showComments() {
        if (!isset($_SESSION['authenticated'])) {
            require_once(ADMIN_VIEW.DS."modules".DS."users".DS."login.php");
            die();
        }

        $commentManager = new CommentManager();
        $comments = $commentManager->findAll();

        $pageTitle = "Liste des commentaires";
        require_once(ADMIN_VIEW.DS."modules".DS."comments".DS."show.php");
    }

    public function showReported() {
        if (!isset($_SESSION['authenticated'])) {
            require_once(ADMIN_VIEW.DS."modules".DS."users".DS."login.php");
            die();
        }

        $commentManager = new CommentManager();
        $reportedComments = $commentManager->selectReported();
        $countReports = $commentManager->countReported();
        $commentManager->removeReport();

        $pageTitle = "Commentaires signalés";
        require_once(ADMIN_VIEW.DS."modules".DS."comments".DS."showReported.php");
    }

    public function deleteComment() {
        if (!isset($_SESSION['authenticated'])) {
            require_once(ADMIN_VIEW.DS."modules".DS."users".DS."login.php");
            die();
        }

        if (!isset($_GET['commentId']) || empty($_GET['commentId']) || !intval($_GET['commentId'])) {
            require_once(TEMP.DS."errors".DS."404.php");
            die();
        }

        $commentManager = new CommentManager();

        $commentManager->find($_GET['commentId']);
        $commentManager->delete($_GET['commentId']);

        header('Location: /app_mvc/admin/showComments');
    }

    public function removeReport() {
        if (!isset($_GET['commentId']) || empty($_GET['commentId']) || !intval($_GET['commentId'])) {
            require_once(TEMP.DS."errors".DS."404.php");
            die();
        }

        $commentManager = new CommentManager();
        $comment = $commentManager->find($_GET['commentId']);

        $commentManager->removeReport();

        header('Location: /app_mvc/admin');
    }
}
