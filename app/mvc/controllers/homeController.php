<?php
/**
 * Created by PhpStorm.
 * User: Saiya
 * Date: 5/3/2017
 * Time: 11:13 AM
 */
require_once(MVC.DS."entity".DS."News.php");
require_once(MVC.DS."entity".DS."User.php");
require_once(MVC . DS . "entity" . DS . "Comment.php");
require_once(MVC.DS."managers".DS."NewsManager.php");
require_once(MVC.DS."managers".DS."CommentManager.php");
require_once(MVC.DS."managers".DS."UserManager.php");

class HomeController extends Controller {

    function __construct($data = array()) {
        parent::__construct($data);
    }

    public function index() {

        $news = new NewsManager();
        $data = $news->getList(1, 4);
        $lastNews = $news->getList(0, 1);

        $pageTitle = "Accueil";

        require_once(MVC . DS . "views" . DS . "home" . DS . "index.php");
    }

    ///////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////// NEWS /////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    public function show() {

        if(!isset($_GET['id']) || empty($_GET['id']) || !intval($_GET['id'])) {
            require_once(TEMP.DS."errors".DS."404.php");
            die();
        }

        $newsManager = new NewsManager();
        $news = $newsManager->find($_GET['id']);


        $comment = new Comment();

        $commentManager = new CommentManager();
        $comment = $commentManager->findAllByArticleId($comment);

        $dataComments = $commentManager->countByArticle($_GET['id']);

        $pageTitle = $news->title();

        require_once(HOME_VIEW.DS."news".DS."show.php");

    }

    public function chapitres() {

        $news = new NewsManager();
        $data = $news->findAll();

        $pageTitle = "Chapitres";

        require_once(HOME_VIEW.DS."news".DS."chapitres.php");
    }

    ///////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////// COMMENTS /////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    public function addComment() {

        if (!isset($_GET['id']) || empty($_GET['id']) || !intval($_GET['id'])) {
            require_once(TEMP.DS."errors".DS."404.php");
            die();
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (!isset($_POST['author']) || empty($_POST['author']) ||
                !isset($_POST['content']) || empty($_POST['content'])) {
                ?><script>alert('Veuillez remplir tous les champs du formulaire');</script><?php
            } else {
                if(array_key_exists('commentParent', $_GET)){
                    $parentId = $_GET['commentParent'];
                } else{
                    $parentId = NULL;
                }

                $comment = new Comment();

                $comment->setAuthor($_POST['author']);
                $comment->setContent($_POST['content']);
                $comment->setDate(new \DateTime());
                $comment->setArticleId($_GET['id']);
                $comment->setParentId($parentId);

                $commentManager = new CommentManager();
                $commentManager->addComment($comment);

                header('Location: /app_mvc/home/show?id=' . $_GET['id'] );
            }
        }
    }

    public function addReport() {
        if (!isset($_GET['commentId']) || empty($_GET['commentId']) || !intval($_GET['commentId'])) {
            require_once(TEMP.DS."errors".DS."404.php");
            die();
        }

        $commentManager = new CommentManager();
        $commentManager->find($_GET['commentId']);
        $commentManager->addReport();

        header( 'refresh:0; url=/app_mvc/home/show?id=' . $_GET['id'] );

        ?> <script>alert("Le commentaire a bien été signalé.");</script> <?php
    }
}
