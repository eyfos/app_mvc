<?php
/**
 * Created by PhpStorm.
 * User: Saiya
 * Date: 6/2/2017
 * Time: 6:21 PM
 */

class NewsManager {

    public function add(News $news) {
            try {
                $dbManager = Db::getInstance();
                $dbManager = $dbManager->getPDOInstance();
                $req = $dbManager->prepare('INSERT INTO news (author, image, title, content, dateAdd, dateModif) VALUES (:author, :image, :title, :content, NOW(), NOW())');

                try {
                    $req->bindValue(':author', $news->author(), PDO::PARAM_STR);
                    $req->bindValue(':title', $news->title(), PDO::PARAM_STR);
                    $req->bindValue(':content', $news->content(), PDO::PARAM_STR);
                    $req->bindValue(':image', $news->image());
                    $req->execute();

                    return $dbManager->lastInsertId();

                } catch(PDOException $e) {
                    echo $e->getMessage();
                }
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        }

    public function update(News $news) {
            try {
                $dbManager = Db::getInstance();
                $dbManager = $dbManager->getPDOInstance();
                $req = $dbManager->prepare('UPDATE news SET author = :author, title = :title, content = :content WHERE id = :id');
                try {
                    $req->bindParam(':id', $_GET['id'], PDO::PARAM_INT);
                    $req->bindParam(':author', $_POST['author'], PDO::PARAM_STR);
                    $req->bindParam(':title', $_POST['title'], PDO::PARAM_STR);
                    $req->bindParam(':content', $_POST['content'], PDO::PARAM_STR);

                    $req->execute();

                } catch(PDOException $e) {
                    echo $e->getMessage();
                }
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        }

    public function find($id) {
        if(!is_string($id)){
            $id = $id->id();
        }
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $req = $dbManager->prepare('SELECT * FROM news WHERE id = :id');

            try {
                $req->bindParam(':id', $id, PDO::PARAM_INT);

                $req->execute();
                $data = $req->fetch(PDO::FETCH_ASSOC);

                if ($req->rowCount() > 0){
                    $news = new News();
                    $news->hydrate($data);

                    return $news;
                } else {
                    require_once(TEMP.DS."errors".DS."404.php");
                    die();
                }
            } catch (PDOException $e) {
                echo $e->getMessage();
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function findAll() {
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $req = $dbManager->prepare('SELECT * FROM news ORDER BY id');

            try {
                $req->execute();
                $datas = $req->fetchAll(PDO::FETCH_ASSOC);
                $data = null;

                foreach ($datas as $oneNews) {
                    $news = new News();
                    $news->hydrate($oneNews);
                    $data[] = $news;
                }

                return $data;
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function findAllNewestFirst() {
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $req = $dbManager->prepare('SELECT * FROM news ORDER BY dateAdd DESC');

            try {
                $req->execute();
                $datas = $req->fetchAll(PDO::FETCH_ASSOC);
                $data = null;

                foreach ($datas as $oneNews) {
                    $news = new News();
                    $news->hydrate($oneNews);
                    $data[] = $news;
                }

                return $data;
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function count() {
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $req = $dbManager->prepare('SELECT COUNT(*) FROM news');

            try {
                $req->execute();
                $data = $req->fetchColumn();

                return $data;
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function delete(News $news) {
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $reqSelect = $dbManager->prepare('SELECT * FROM news');
            $req = $dbManager->prepare('DELETE FROM news WHERE id = :id');

            try {
                $reqSelect->execute();

                $req->bindParam(':id', $_GET['id'], PDO::PARAM_INT);
                $req->execute(array('id' => $news->id()));

            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function getList($debut = -1, $limite = -1) {
        $data = "";
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $query = 'SELECT * FROM news ORDER BY id DESC';
            if ($debut != -1 || $limite != -1) {
                $query .= ' LIMIT '.(int) $limite.' OFFSET '.(int) $debut;
            }
            try {
                $req = $dbManager->prepare($query);
                $req->execute();
                $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'News');

                $data = $req->fetchAll();

                $req->closeCursor();
            } catch(PDOException $e) {

                echo $e->getMessage();
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }

        return $data;
    }
}