<?php
/**
 * Created by PhpStorm.
 * User: Saiya
 * Date: 6/4/2017
 * Time: 2:23 AM
 */

class UserManager {

    public function add(User $user) {
            try {
                $dbManager = Db::getInstance();
                $dbManager = $dbManager->getPDOInstance();
                $req = $dbManager->prepare('INSERT INTO users (username, password, image) VALUES (:username, :password, :image)');

                try {
                    $req->bindValue(':username', $user->username(), PDO::PARAM_STR);
                    $req->bindValue(':password', $user->password(), PDO::PARAM_STR);
                    $req->bindValue(':image', $user->image());
                    $req->execute();

                    return $dbManager->lastInsertId();

                } catch(PDOException $e) {
                    echo $e->getMessage();
                }
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        }

    public function update(User $user) {
            try {
                $dbManager = Db::getInstance();
                $dbManager = $dbManager->getPDOInstance();
                $req = $dbManager->prepare('UPDATE users SET username = :username, password = :password WHERE id = :id');
                $password = sha1('$^";=R6k)aG}9pU' . $_POST['password']);
                try {
                    $req->bindParam(':username', $_POST['username'], PDO::PARAM_STR);
                    $req->bindParam(':password', $password, PDO::PARAM_STR);
                    $req->bindParam(':id', $_GET['id'], PDO::PARAM_INT);

                    $req->execute();

                } catch(PDOException $e) {
                    echo $e->getMessage();
                }
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        }

    public function delete(User $user) {
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $req = $dbManager->prepare('DELETE FROM users WHERE id = :id');

            try {
                $req->bindParam(':id', $_GET['id'], PDO::PARAM_INT);
                $req->execute(array('id' => $user->id()));
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function count() {
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $req = $dbManager->prepare('SELECT COUNT(*) FROM users');

            try {
                $req->execute();
                $data = $req->fetchColumn();

                return $data;
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function find($id) {
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $req = $dbManager->prepare('SELECT * FROM users WHERE id = :id');

            try {
                $req->bindParam(':id', $id, PDO::PARAM_INT);

                $req->execute();
                $data = $req->fetch(PDO::FETCH_ASSOC);

                if ($req->rowCount() > 0){
                    $user = new User();
                    $user->hydrate($data);
                    return $user;
                } else {
                    require_once(TEMP.DS."errors".DS."404.php");
                    die();
                }
            } catch (PDOException $e) {
                echo $e->getMessage();
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function findAll() {
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $req = $dbManager->prepare('SELECT * FROM users ORDER BY id');

            try {
                $req->execute();
                $datas = $req->fetchAll(PDO::FETCH_ASSOC);
                $data = null;

                foreach ($datas as $oneUser) {
                    $user = new User();
                    $user->hydrate($oneUser);
                    $data[] = $user;
                }

                return $data;
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function login(User $user) {
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $req = $dbManager->prepare('SELECT * FROM users WHERE username = :username AND password = :password');
            try {
//                $req->bindParam(':username', $user->username(), PDO::PARAM_STR);
//                $req->bindParam(':password', $user->password(), PDO::PARAM_STR);
                $req->execute(array(
                    'username' => $user->username(),
                    'password' => sha1('$^";=R6k)aG}9pU' . $user->password())
                ));
                $data = $req->fetch();

                if ($req->rowCount() > 0) {
                    $_SESSION['username'] = $data['username'];
                    $_SESSION['image'] = $data['image'];
                    $_SESSION['authenticated'] = true;
                } else {
                    echo "Pseudo ou mot de passe incorrect";
                    die();
                }
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }

        return true;
    }

    public function logout() {
        session_destroy();

        $_SESSION = array();
        setcookie('login', '');
        setcookie('password', '');

        header('Location: /app_mvc/home/');
    }

    public function getUserImage($username) {
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $req = $dbManager->prepare('SELECT image FROM users WHERE username = :username');
            try {
                $req->bindParam(':username', $username, PDO::PARAM_STR);
                $data = $req->execute();

                print_r($data);
                return $data;
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

}