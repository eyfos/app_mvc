<?php
/**
 * Created by PhpStorm.
 * User: Saiya
 * Date: 6/4/2017
 * Time: 1:41 AM
 */

class CommentManager {

    public function addComment(Comment $comment) {
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $req = $dbManager->prepare('INSERT INTO comments SET articleId = :articleId, author = :author, content = :content, report = :report, date = NOW(), commentParent = :parentId');

            try {
                $req->bindValue(':articleId', $comment->articleId());
                $req->bindValue(':author', $comment->author());
                $req->bindValue(':content', $comment->content());
                $req->bindValue(':parentId', $comment->parentId());
                $req->bindValue(':report', $comment->report());
                $req->execute();

                return $dbManager->lastInsertId();

            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function findAllByArticleId(Comment $comment) {
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $req = $dbManager->prepare('SELECT * FROM comments WHERE articleId = :articleId ORDER BY date');

            try {
                $req->bindValue(':articleId', $comment->articleId());
                $req->execute(array('articleId' => $_GET['id']));
                $data = $req->fetchAll();

                return $data;

            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function find($commentId) {
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $req = $dbManager->prepare('SELECT * FROM comments WHERE commentId = :commentId');

            try {
                $req->bindParam(':commentId', $commentId, PDO::PARAM_INT);

                $req->execute();
                $data = $req->fetch(PDO::FETCH_ASSOC);

                if ($req->rowCount() > 0){
                    $comment = new Comment();
                    $comment->hydrate($data);
                    return $comment;
                } else {
                    require_once(TEMP.DS."errors".DS."404.php");
                    die();
                }
            } catch (PDOException $e) {
                echo $e->getMessage();
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function update(Comment $comment) {
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $req = $dbManager->prepare('UPDATE comments SET content = :content WHERE commentId = :commentId');

            try {
                $req->bindParam(':commentId', $_GET['commentId'], PDO::PARAM_INT);
                $req->bindParam(':content', $_POST['content'], PDO::PARAM_STR);

                $req->execute();
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function delete($commentId) {
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $reqSelect = $dbManager->prepare('SELECT * FROM comments');
            $req = $dbManager->prepare('DELETE FROM comments WHERE commentId = :commentId');

            try {
                $reqSelect->execute();

                $req->bindParam(':commentId', $commentId, PDO::PARAM_INT);
                $req->execute();
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function count() {
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $req = $dbManager->prepare('SELECT COUNT(*) FROM comments');

            try {
                $req->execute();
                $data = $req->fetchColumn();

                return $data;
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function countReported() {
        $reported = 1;
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $req = $dbManager->prepare('SELECT COUNT(*) FROM comments WHERE report = :report');

            try {
                $req->bindParam(':report', $reported, PDO::PARAM_INT);
                $req->execute();
                $data = $req->fetchColumn();

                return $data;
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function countByArticle() {
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $req = $dbManager->prepare('SELECT COUNT(*) FROM comments WHERE articleId = :articleId');

            try {
                $req->bindValue(':articleId', $_GET['id']);
                $req->execute();
                $data = $req->fetchColumn();

                return $data;
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function findAll() {
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $req = $dbManager->prepare('SELECT * FROM comments ORDER BY commentId');

            try {
                $req->execute();
                $datas = $req->fetchAll(PDO::FETCH_ASSOC);
                $data = null;

                foreach ($datas as $oneComment) {
                    $comment = new Comment();
                    $comment->hydrate($oneComment);
                    $data[] = $comment;
                }

                return $data;
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function findAllReported() {
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $req = $dbManager->prepare('SELECT * FROM comments ORDER BY commentId');

            try {
                $req->execute();
                $datas = $req->fetchAll(PDO::FETCH_ASSOC);
                $data = null;

                foreach ($datas as $oneComment) {
                    $comment = new Comment();
                    $comment->hydrate($oneComment);
                    $data[] = $comment;
                }

                return $data;
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function addReport() {
        $reported = 1;
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $req = $dbManager->prepare('UPDATE comments SET report = :report WHERE commentId = :commentId');

            try {
                $req->bindParam(':commentId', $_GET['commentId'], PDO::PARAM_INT);
                $req->bindParam(':report', $reported, PDO::PARAM_INT);

                $req->execute();
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function selectReported() {
        try {
            $reported = 1;
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $req = $dbManager->prepare('SELECT * FROM comments WHERE report = :report');

            try {
                $req->bindParam(':report', $reported, PDO::PARAM_INT);

                $req->execute();
                $reportedComments = $req->fetchAll();

                return $reportedComments;

            } catch (PDOException $e) {
                echo $e->getMessage();
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function removeReport() {
        $reported = 0;
        try {
            $dbManager = Db::getInstance();
            $dbManager = $dbManager->getPDOInstance();
            $req = $dbManager->prepare('UPDATE comments SET report = :report WHERE commentId = :commentId');

            try {
                $req->bindParam(':commentId', $_GET['commentId'], PDO::PARAM_INT);
                $req->bindParam(':report', $reported, PDO::PARAM_INT);

                $req->execute();
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
}