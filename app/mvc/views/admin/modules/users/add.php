<?php include_once(TEMP . DS . "headerAdmin.php"); ?>

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showImg')
                    .attr('src', e.target.result);
                document.getElementById('showImg').style.width = "100%";
                document.getElementById('showImg').style.display = "block";
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

        <div class="addNews">
            <div class="row">
                <div clas="col-md-12">
                    <div class="adminPageTitle">
                        <h2>Ajouter un utilisateur</h2>
                        <br/>
                        <span><i class="fa fa-pencil" aria-hidden="true"></i> Nouveau membre</span>
                    </div>
                    <div class="addNewsForm">
                        <form class="form-horizontal" action="/app_mvc/admin/addUser" enctype="multipart/form-data" method="post">
                            <fieldset>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="textinput">Pseudo</label>
                                        <div class="col-md-9">
                                            <input id="textinput" name="username" placeholder="Pseudo" class="form-control input-md" required type="text" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="textinput">Mot de passe</label>
                                        <div class="col-md-9">
                                            <input type="password" id="password" name="password" placeholder="Mot de passe" class="form-control input-md" required />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="textinput">Image</label>
                                        <div class="col-md-9">
                                            <input type="file" onchange="readURL(this);" name="image" accept="image/*" required />
                                            <img id="showImg" src="#" style="display:none;"/>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <button type="submit" class="btn btn-default">Ajouter un utilisateur</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- viewZone -->
</div> <!-- container-fluid -->

</body>
</html>