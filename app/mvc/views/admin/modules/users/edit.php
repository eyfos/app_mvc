<?php include_once(TEMP . DS . "headerAdmin.php"); ?>

        <div class="editUser">
            <div class="row">
                <div class="col-md-12">
                    <div class="adminPageTitle">
                        <h2>Modifier un utilisateur</h2>
                        <br/>
                        <span><i class="fa fa-pencil" aria-hidden="true"></i> Modifier le membre (<?php echo htmlspecialchars($user->id()); ?>) : <?php echo htmlspecialchars($user->username()) ?></span>
                    </div>
                    <div class="editUserForm">
                        <form class="form-horizontal" action="/app_mvc/admin/editUser?id=<?php echo htmlspecialchars($user->id()); ?>"  enctype="multipart/form-data" method="post">
                            <fieldset>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="textinput">Pseudo</label>
                                        <div class="col-md-9">
                                            <input type="text" name="username" value="<?php echo $user->username(); ?>" class="form-control input-md" required/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="textinput">Mot de passe *</label>
                                        <div class="col-md-9">
                                            <input type="password" id="password" name="password" placeholder="Saisir le mot de passe" class="form-control input-md" required />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="textinput">Image</label>
                                        <div class="col-md-9">
                                            <img src="/app_mvc/web/img/users/<?php echo $user->image() ?>" class="img-fluid" />
                                            <br/>
                                            <input type="file" onchange="readURL(this);" name="image" accept="image/*" />
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <button type="submit" class="btn btn-default">Modifier l'utilisateur</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- viewZone -->
</div> <!-- container-fluid -->

</body>
</html>