<?php include_once(TEMP . DS . "headerAdmin.php"); ?>

        <div class="usersAdmin">
            <div class="row">
                <div class="adminPageTitle">
                    <h2>Liste des utilisateurs</h2>
                    <br/>
                    <span><i class="fa fa-pencil" aria-hidden="true"></i> Tableau des membres</span>
                </div>
                <div class="col-md-12 tableNews">
                    <div class="tableHeader">
                        <div class="tableInfos">
                            <div class="row">
                                <div class="col-md-2 text-center">
                                    <span>Image</span>
                                </div>
                                <div class="col-md-6 text-center">
                                    <span>Pseudo</span>
                                </div>
                                <div class="col-md-1 text-center">
                                    <span>ID <i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                                </div>
                            </div>
                        </div>
                        <?php
                        if (isset($users)) {
                            foreach ($users as $user) {
                                ?>
                                <div class="tableRow">
                                    <div class="col-md-12 tableElement">
                                        <a href="#">
                                            <div class="col-md-2">
                                                <div class="tableAuthor">
                                                    <img src="/app_mvc/web/img/users/<?php echo $user->image(); ?>" class="img-fluid" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="tableTitle">
                                                    <span><?php echo htmlspecialchars($user->username()) ?></span>
                                                </div>
                                            </div>
                                            <div class="col-md-1 text-center">
                                                <div class="tableDate">
                                                    <?php echo htmlspecialchars($user->id()); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2 text-center">
                                                <div class="tableDate">

                                                </div>
                                            </div>
                                        </a>

                                        <div class="col-md-1">
                                            <span class="deleteIcon">
                                                <a href="/app_mvc/admin/deleteUser?id=<?php echo htmlspecialchars($user->id()); ?>">
                                                    <i class="fa fa-times" aria-hidden="true"></i>
                                                </a>
                                            </span>

                                            <span class="editIcon">
                                                <a href="/app_mvc/admin/editUser?id=<?php echo htmlspecialchars($user->id()); ?>">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            } /* foreach */
                        } else { ?>
                            <div class="noData">
                                <p>Aucun utilisateur à afficher</p>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>

    </div> <!-- viewZone -->
</div> <!-- container-fluid -->

</body>
</html>
