<?php require_once(TEMP.DS."default.php"); ?>

<div class="loginForm">
    <div class="corner">
        <i class="fa fa-sign-in" aria-hidden="true"></i>
    </div>
    <div class="form">
        <form action="/app_mvc/admin/login" method="post">
            <h2>Connexion au compte</h2>

            <label for="username" name="username">
                Pseudo :
            </label>
            <br><br>
            <input type="text" name="username" placeholder="Pseudo" />

            <label for="password" name="password">
                Mot de passe :
            </label>
            <br><br>
            <input type="password" name="password" placeholder="Mot de passe" />
            <br>

            <button type="submit">Envoyer</button>
        </form>
    </div>

    <div class="forget">
        <a href="#">Mot de passe oublié ?</a>
    </div>
</div>

</body>
</html>