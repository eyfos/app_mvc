<?php include_once(TEMP . DS . "headerAdmin.php"); ?>

        <div class="commentsAdmin">
            <div class="row">
                <div class="adminPageTitle">
                    <h2>Liste des commentaires</h2>
                    <br/>
                    <span><i class="fa fa-pencil" aria-hidden="true"></i> Tableau des commentaires</span>
                </div>
                <div class="col-md-12 tableNews">
                    <div class="tableHeader">
                        <div class="tableInfos">
                            <div class="row">
                                <div class="col-md-2 text-center">
                                    <span>Auteur</span>
                                </div>
                                <div class="col-md-5 text-center">
                                    <span>Contenu</span>
                                </div>
                                <div class="col-md-1 text-center">
                                    <span>ID <i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                                </div>
                                <div class="col-md-1 text-center">
                                    <span>Article <i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                                </div>
                                <div class="col-md-2 text-center">
                                    <span>Date <i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                                </div>
                            </div>
                        </div>

                        <?php
                        if (isset($comments)) {
                            foreach ($comments as $comment) {
                                ?>
                                <div class="tableRow">
                                    <div class="col-md-12 tableElement">
                                            <div class="col-md-2">
                                                <div class="tableAuthor">
                                                    <img src="/app_mvc/web/img/default_user.png" class="img-fluid" />
                                                    <span><?php echo htmlspecialchars($comment->author()); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="tableContent">
                                                    <?php $content = strip_tags($comment->content());
                                                    if (strlen($content) > 150) {
                                                        $stringCut = substr($content, 0, 150);
                                                        $content = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
                                                    } ?>
                                                    <?php echo $content; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-1 text-center">
                                                <div class="tableCommentId">
                                                    <a href="/app_mvc/home/show?id=<?php echo $comment->articleId() ?>#commentMain_<?php echo $comment->commentId() ?>">
                                                        <?php echo htmlspecialchars($comment->commentId()); ?>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-1 text-center">
                                                <div class="tableArticleId">
                                                    <a href="/app_mvc/home/show?id=<?php echo $comment->articleId() ?>">
                                                        <?php echo htmlspecialchars($comment->articleId()); ?>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-2 text-center">
                                                <div class="tableDate">
                                                    <?php $commentsDate = strtotime($comment->date()); ?>
                                                    <?php echo $commentsDateFormat = date("j M Y", $commentsDate); ?>
                                                </div>
                                            </div>
                                        </a>

                                        <div class="col-md-1 actions">
                                            <span class="deleteIcon">
                                                   <a href="/app_mvc/admin/deleteComment?commentId=<?php echo htmlspecialchars($comment->commentId()); ?>">
                                                       <i class="fa fa-times" aria-hidden="true"></i>
                                                   </a>
                                            </span>

                                            <span class="editIcon">
                                                <a href="/app_mvc/admin/editComment?commentId=<?php echo htmlspecialchars($comment->commentId()); ?>">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            } /* foreach */
                        } else { ?>
                            <div class="noData">
                                <p>Aucun commentaire à afficher</p>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- viewZone -->
</div> <!-- container-fluid -->

</body>
</html>
