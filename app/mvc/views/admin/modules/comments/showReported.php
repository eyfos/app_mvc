<?php include_once(TEMP . DS . "headerAdmin.php"); ?>

        <div class="commentsAdmin">
            <div class="row">
                <div class="adminPageTitle">
                    <h2>Liste des commentaires signalés</h2>
                    <br/>
                    <span><i class="fa fa-pencil" aria-hidden="true"></i> Tableau des commentaires signalés</span>
                </div>
                <div class="col-md-12 tableNews">
                    <div class="tableHeader">
                        <div class="tableInfos">
                            <div class="row">
                                <div class="col-md-2 text-center">
                                    <span>Auteur</span>
                                </div>
                                <div class="col-md-4 text-center">
                                    <span>Contenu</span>
                                </div>
                                <div class="col-md-3 text-center">
                                    <span>Valider le commentaire <i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                                </div>
                                <div class="col-md-3 text-center">
                                    <span>Supprimer le commentaire <i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                                </div>
                            </div>
                        </div>
                        <?php
                        if (!empty($reportedComments)) {
                            foreach ($reportedComments as $reported) {
                                ?>
                                <div class="tableRow">
                                    <div class="col-md-12 tableElement">
                                            <div class="col-md-2">
                                                <div class="tableAuthor">
                                                    <img src="/app_mvc/web/img/default_user.png" class="img-fluid" />
                                                    <span><?php echo htmlspecialchars($reported['author']); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="tableContent">
                                                    <?php $content = strip_tags($reported['content']);
                                                    if (strlen($content) > 150) {
                                                        $stringCut = substr($content, 0, 150);
                                                        $content = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
                                                    } ?>
                                                    <?php echo $content; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <span class="verifReport showReported">
                                                    <a href="/app_mvc/admin/removeReport?commentId=<?php echo htmlspecialchars($reported['commentId']); ?>">
                                                        Valider <i class="fa fa-check" aria-hidden="true"></i>
                                                    </a>
                                                </span>
                                            </div>
                                            <div class="col-md-3">
                                                <span class="deleteIcon showReported">
                                                    <a href="/app_mvc/admin/deleteComment?commentId=<?php echo htmlspecialchars($reported['commentId']); ?>">
                                                        Supprimer <i class="fa fa-times" aria-hidden="true"></i>
                                                    </a>
                                                </span>
                                            </div>
                                    </div>
                                </div>
                                <?php
                            } /* foreach */
                        } else { ?>
                            <div class="noData">
                                <p>Aucun commentaire signalé à afficher</p>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- viewZone -->
</div> <!-- container-fluid -->

</body>
</html>
