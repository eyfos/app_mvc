<?php include_once(TEMP . DS . "headerAdmin.php"); ?>

        <div class="editComment">
            <div class="row">
                <div clas="col-md-12">
                    <div class="adminPageTitle">
                        <h2>Modifier un commentaire</h2>
                        <br/>
                        <span><i class="fa fa-pencil" aria-hidden="true"></i> Modifier le commentaire numéro - <?php echo htmlspecialchars($comment->commentId()); ?></h1></span>
                    </div>
                    <div class="addNewsForm">

                        <form class="form-horizontal" action="/app_mvc/admin/editComment?commentId=<?php echo htmlspecialchars($comment->commentId()); ?>" method="post">
                            <fieldset>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="textinput">Auteur</label>
                                        <div class="col-md-9">
                                            <div class="authorEdit"><?php echo htmlspecialchars($comment->author()); ?></div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="content">Contenu</label>
                                        <div class="col-md-9">
                                                    <textarea class="form-control input-md" name="content" id="content" data-title="Le contenu de l'article est obligatoire">
                                                        <?php echo htmlspecialchars($comment->content()); ?>
                                                    </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                </div>
                            </fieldset>
                            <button type="submit" class="btn btn-default">Modifier le commentaire</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div> <!-- viewZone -->
</div> <!-- container-fluid -->
</body>
</html>