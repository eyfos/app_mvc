<?php include_once(TEMP . DS . "headerAdmin.php"); ?>

        <div class="editNews">
            <div class="row">
                <div clas="col-md-12">
                    <div class="adminPageTitle">
                        <h2>Modifier une news</h2>
                        <br/>
                        <span><i class="fa fa-pencil" aria-hidden="true"></i> Modifier l'article (<?php echo htmlspecialchars($news->id()); ?>) : <?php echo htmlspecialchars($news->title()); ?></span>
                    </div>
                    <div class="editNewsForm">

                        <form class="form-horizontal" action="/app_mvc/admin/editNews?id=<?php echo htmlspecialchars($news->id()); ?>"  enctype="multipart/form-data" method="post">
                            <fieldset>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="textinput">Auteur</label>
                                        <div class="col-md-9">
                                            <div class="authorEdit">
                                                <?php echo $news->author() ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="textinput">Titre</label>
                                        <div class="col-md-9">
                                            <input id="textinput" name="title" value="<?php echo htmlspecialchars($news->title()); ?>" class="form-control input-md" required type="text">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="content">Contenu</label>
                                        <div class="col-md-9">
                                            <textarea class="form-control input-md" name="content" id="content" data-title="Le contenu de l'article est obligatoire">
                                                <?php echo htmlspecialchars($news->content()); ?>
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="textinput">Image</label>
                                        <div class="col-md-9">
                                            <img src="/app_mvc/web/img/news/<?php echo $news->image(); ?>" class="img-fluid" />
                                            <br/>
                                            <input type="file" onchange="readURL(this);" name="image" accept="image/*" />
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <button type="submit" class="btn btn-default">Modifier l'article</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- viewZone -->
</div> <!-- container-fluid -->

    </body>
    </html>

