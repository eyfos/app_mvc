<?php include_once(TEMP . DS . "headerAdmin.php"); ?>

        <div class="newsAdmin">
            <div class="row">
                <div class="adminPageTitle">
                    <h2>Liste des news</h2>
                    <br/>
                    <span><i class="fa fa-pencil" aria-hidden="true"></i> Tableau des articles</span>
                </div>
                <div class="col-md-12 tableNews">
                    <div class="tableHeader">
                        <div class="tableInfos">
                            <div class="row">
                                <div class="col-md-2 text-center">
                                    <span>Auteur</span>
                                </div>
                                <div class="col-md-6 text-center">
                                    <span>Contenu</span>
                                </div>
                                <div class="col-md-1 text-center">
                                    <span>ID <i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                                </div>
                                <div class="col-md-2 text-center">
                                    <span>Date <i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                                </div>
                            </div>
                        </div>

                        <?php
                        if (isset($allNews)) {
                            foreach ($allNews as $news) { ?>
                                <div class="tableRow">
                                    <div class="col-md-12 tableElement">
                                        <a href="/app_mvc/home/show?id=<?php echo htmlspecialchars($news->id()) ?>">
                                            <div class="col-md-2">
                                                <div class="tableAuthor">
                                                    <img src="/app_mvc/web/img/news/<?php echo htmlspecialchars($news->image()); ?>" class="img-fluid" />
                                                    <span><?php echo htmlspecialchars($news->author()); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="tableTitle">
                                                  <span>
                                                      <?php echo htmlspecialchars($news->title()) ?>
                                                  </span>
                                                </div>
                                                <div class="tableContent">
                                                    <?php $content = strip_tags($news->content());
                                                    if (strlen($content) > 150) {
                                                        $stringCut = substr($content, 0, 150);
                                                        $content = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
                                                    } ?>
                                                    <?php echo htmlspecialchars($content); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-1 text-center">
                                                <div class="tableDate">
                                                    <?php echo htmlspecialchars($news->id()); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2 text-center">
                                                <div class="tableDate">
                                                    <?php $newsDate = strtotime($news->dateAdd()); ?>
                                                    <?php echo htmlspecialchars($newsDateFormat = date("j M Y", $newsDate)); ?>
                                                </div>
                                            </div>
                                        </a>

                                        <div class="col-md-1">
                                            <span class="deleteIcon">
                                                <a href="/app_mvc/admin/deleteNews?id=<?php echo htmlspecialchars($news->id()); ?>">
                                                    <i class="fa fa-times" aria-hidden="true"></i>
                                                </a>
                                            </span>

                                            <span class="editIcon">
                                                <a href="/app_mvc/admin/editNews?id=<?php echo htmlspecialchars($news->id()); ?>">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            } /* foreach */
                        } else { ?>
                            <div class="noData">
                                <p>Aucun article à afficher</p>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- viewZone -->
</div> <!-- container-fluid -->

</body>
</html>