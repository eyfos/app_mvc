<?php include_once(TEMP . DS . "headerAdmin.php"); ?>
        <div class="newsAdmin">
            <div class="dataOverview">
                <div class="row">
                    <div class="col-md-12">
                        <div class="adminPageTitle">
                            <h2>Panneau d'administration</h2>
                            <span><i class="fa fa-tachometer" aria-hidden="true"></i> Vue d'ensemble</span>
                        </div>
                        <div class="col-md-4">
                            <div class="dataBox">
                                <a href="#">
                                    <div class="dataBoxHeading dark-red">
                                        <i class="fa fa-pencil fa-fw fa-3x"></i>
                                    </div>
                                </a>
                                <div class="dataBoxContent dark-red">
                                    <div class="dataBoxDescription text-faded">
                                        News
                                    </div>
                                    <div class="dataBoxNumber text-faded">
                                        <?php echo htmlspecialchars($data['news']) ?>
                                        <span id="sparklineA"></span>
                                    </div>
                                    <a href="/app_mvc/admin/showNews" class="dataBoxFooter">Voir les news <i class="fa fa-chevron-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="dataBox">
                                <a href="#">
                                    <div class="dataBoxHeading dark-green">
                                        <i class="fa fa-comments fa-fw fa-3x"></i>
                                    </div>
                                </a>
                                <div class="dataBoxContent dark-green">
                                    <div class="dataBoxDescription text-faded">
                                        Commentaires
                                    </div>
                                    <div class="dataBoxNumber text-faded">
                                        <?php echo htmlspecialchars($data['Comment']) ?>
                                        <span id="sparklineA"></span>
                                    </div>
                                    <a href="/app_mvc/admin/showComments" class="dataBoxFooter">Voir les commentaires <i class="fa fa-chevron-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="dataBox">
                                <a href="#">
                                    <div class="dataBoxHeading dark-purple">
                                        <i class="fa fa-users fa-fw fa-3x"></i>
                                    </div>
                                </a>
                                <div class="dataBoxContent dark-purple">
                                    <div class="dataBoxDescription text-faded">
                                        Utilisateurs
                                    </div>
                                    <div class="dataBoxNumber text-faded">
                                        <?php echo htmlspecialchars($data['users']) ?>
                                        <span id="sparklineA"></span>
                                    </div>
                                    <a href="/app_mvc/admin/showUsers" class="dataBoxFooter">Voir les utilisateurs <i class="fa fa-chevron-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- viewZone -->
</div> <!-- container-fluid -->


</body>
</html>