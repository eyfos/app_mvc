<?php
/**
 * Created by PhpStorm.
 * User: Saiya
 * Date: 7/13/2017
 * Time: 12:25 PM
 */

include_once(TEMP . DS . "default.php");
include_once(TEMP . DS . "headerHome.php"); ?>

        <div class="navBarmenu">
            <nav class="navbar navbar-default navbar-custom">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div>
                        <div class="collapse navbar-collapse" id="myNavbar">
                            <ul class="nav navbar-nav">
                                <li><a href="/app_mvc/home#news"><i class="fa fa-home" aria-hidden="true"></i> Accueil</a></li>
                                <li><a href="/app_mvc/home#about">à propos</a></li>
                                <li><a href="/app_mvc/home#bio">Biographie</a></li>
                                <li><a href="/app_mvc/home#presse">Presse</a></li>
                                <li><a href="/app_mvc/home#contact">Contact</a></li>
                            </ul>

                            <ul class="nav navbar-nav pull-right">
                                <li><a href="/app_mvc/home/chapitres"><i class="fa fa-book" aria-hidden="true"></i> Lire les chapitres</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </div>

        <div class="container">
            <div class="chapterPage">
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        foreach ($data as $chapter) {
                            ?>
                            <div class="col-md-6">
                                <div class="chapterSingle">
                                    <a href='/app_mvc/home/show?id=<?php echo htmlspecialchars($chapter->id()); ?>'>
                                        <div class="chapterImage">
                                            <img src="/app_mvc/web/img/news/<?php echo $chapter->image() ?>" class="img-fluid" />
                                        </div>
                                        <div class="newsContainer">
                                            <div class="chapterTitle"><?php $title = strip_tags($chapter->title());
                                                if (strlen($title) > 10) {
                                                    $stringCut = substr($title, 0, 50);
                                                    $title = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
                                                } ?>
                                                <p><?php echo $title; ?></p>
                                            </div>
                                            <div class="chapterContent">
                                                <?php $content = strip_tags($chapter->content());
                                                if (strlen($content) > 200) {
                                                    $stringCut = substr($content, 0, 200);
                                                    $content = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
                                                } ?>
                                                <?php echo htmlspecialchars($content); ?>
                                            </div>
                                            <div class="chapterDate">
                                                <?php $chapterDate = strtotime($chapter->dateAdd()); ?>
                                                <em><?php echo htmlspecialchars($chapterDateFormat = date("j M Y", $chapterDate)); ?></em>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer">
            <span>© <?php echo date("Y"); ?> Copyright - <?php echo Config::get("siteName"); ?></span>
        </footer>

    </div> <!-- mainContainer -->
</body>
</html>
