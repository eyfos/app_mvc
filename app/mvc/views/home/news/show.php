<?php include_once(TEMP . DS . "default.php"); ?>
<?php include_once(TEMP . DS . "headerHome.php"); ?>

        <div class="navBarmenu">
            <nav class="navbar navbar-default navbar-custom">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div>
                        <div class="collapse navbar-collapse" id="myNavbar">
                            <ul class="nav navbar-nav">
                                <li><a href="/app_mvc/home#news"><i class="fa fa-home" aria-hidden="true"></i> Accueil</a></li>
                                <li><a href="/app_mvc/home#about">à propos</a></li>
                                <li><a href="/app_mvc/home#bio">Biographie</a></li>
                                <li><a href="/app_mvc/home#presse">Presse</a></li>
                                <li><a href="/app_mvc/home#contact">Contact</a></li>
                            </ul>

                            <ul class="nav navbar-nav pull-right">
                                <li><a href="/app_mvc/home/chapitres"><i class="fa fa-book" aria-hidden="true"></i> Lire les chapitres</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="newsSinglePage">
                        <div class="newsHeader">
                            <div class="newsImage">
                                <img src="/app_mvc/web/img/news/<?php echo $news->image() ?>" class="img-fluid"/>
                            </div>
                            <div class="newsTitle"><h3><?php echo htmlspecialchars($news->title()); ?></h3></div>
                            <div class="newsAuthor">
                                <img src="/app_mvc/web/img/5_thumb" class="img-fluid"/>
                                <span class="articleAuthor">
                                    <?php echo htmlspecialchars($news->author()); ?>
                                </span>
                                <span class="articleDate">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                    <?php $newsDate = strtotime($news->dateAdd()); ?>
                                    <?php echo htmlspecialchars($newsDateFormat = date("j M Y", $newsDate)); ?>
                                </span>
                                <span class="articleComments">
                                    <i class="fa fa-comment-o" aria-hidden="true"></i>
                                    <?php
                                    if ($dataComments == 0) {
                                        echo "Aucun commentaire";
                                    } else if ($dataComments == 1) {
                                        echo htmlspecialchars($dataComments) ?> commentaire <?php
                                    } else {
                                        echo htmlspecialchars($dataComments) ?> commentaires <?php
                                    } ?>
                                </span>
                            </div>
                            <div class="newsHeaderTriangle"></div>
                        </div>
                        <div class="newsContent"><?php echo strip_tags($news->content()); ?></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="comments">
                    <div class="commentsContainer container">
                        <h2>Commentaires</h2>


                        <form class="addComment"
                              action="/app_mvc/home/addComment?id=<?php echo htmlspecialchars($news->id()) ?>" method="post">

                            <?php if (isset($_SESSION['authenticated']) && $_SESSION['authenticated'] == true) { ?>
                                <input type="hidden" name="author" class="author" value="<?php echo htmlspecialchars($_SESSION['username']); ?>"/>
                                <?php
                            } else { ?>
                                <input type="text" name="author" class="author" placeholder="Pseudo" required/>
                            <?php } ?>

                            <input type="hidden" name="articleId" value="<?php echo htmlspecialchars($news->id()) ?>"/>

                            <br/>

                            <textarea type="text" name="content" class="content" rows="3" cols="50" placeholder="Nouveau commentaire"></textarea>

                            <br/>

                            <input type="submit" value="Commenter" class="submitComment"/>
                        </form>

                        <span class="commentsSeparator">
                            <?php
                            if ($dataComments == 0) {
                                echo "Aucun commentaire";
                            } else if ($dataComments == 1) {
                                echo htmlspecialchars($dataComments) ?> commentaire
                                <?php
                            } else {
                                echo htmlspecialchars($dataComments) ?> commentaires
                                <?php
                            }
                            ?>
                        </span>

                        <ul id="commentsList" class="commentsList">
                            <?php
                            foreach ($comment as $comments) {
                                if (NULL == $comments['commentParent']) {
                                    ?>

                                    <!-- Comment Parent - niveau 0 -->
                                    <li class="levelZero">
                                        <div id="commentMain_<?php echo htmlspecialchars($comments['commentId']); ?>"
                                             class="commentMain">
                                            <div class="commentAvatar">
                                                <img src="/app_mvc/web/img/default_user.png" class="img-fluid"/>
                                            </div>
                                            <div class="commentBox" id="commentBox">
                                                <div class="commentHead">
                                                    <h6 class="commentName commentAuthor">
                                                        <?php echo htmlspecialchars($comments['author']); ?>
                                                    </h6>
                                                    <span class="commentDate">
                                                        <?php $newsDate = strtotime($comments['date']); ?>
                                                        <?php echo htmlspecialchars($newsDateFormat = date("j M Y", $newsDate)); ?>
                                                    </span>
                                                    <div id="replyIcon_<?php echo htmlspecialchars($comments['commentId']); ?>">
                                                        <a role="button" data-toggle="collapse"
                                                           href="#replyCommentForm_<?php echo htmlspecialchars($comments['commentId']) ?>"
                                                           aria-expanded="false"
                                                           aria-controls="replyCommentForm_<?php echo htmlspecialchars($comments['commentId']); ?>">
                                                            <i class="fa fa-reply"></i>
                                                        </a>
                                                    </div>

                                                    <a role="button"
                                                       href="/app_mvc/home/addReport?commentId=<?php echo htmlspecialchars($comments['commentId']) ?>&id=<?php echo $comments['articleId'] ?>">
                                                        <i class="fa fa-flag"></i>
                                                    </a>
                                                </div>
                                                <div id="commentContent" class="commentContent">
                                                    <span><?php echo strip_tags($comments['content']); ?></span>
                                                </div>
                                            </div>
                                        </div>

                                        <script>
                                            document.getElementById("replyIcon_<?php echo htmlspecialchars($comments['commentId']); ?>").onclick = function (e) {
                                                e = e || event;
                                                var target = e.target || e.srcElement;
                                                innerId = target.id;

                                                function insertAfter(referenceNode, newNode) {
                                                    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
                                                }

                                                $(document.getElementById("replyIcon_<?php echo htmlspecialchars($comments['commentId']); ?>")).on('click', function () {
                                                    $('.collapse').collapse('hide');
                                                });

                                                var el = document.createElement("div");
                                                el.innerHTML = '<div class="collapse" id="replyCommentForm_<?php echo htmlspecialchars($comments['commentId']); ?>"> <form class="addCommentReply" action="/app_mvc/home/addComment?id=<?php echo htmlspecialchars($news->id()) ?>&commentParent=<?php echo htmlspecialchars($comments['commentId']); ?>" method="post">  <input type="text" name="author" class="author" value="<?php if (isset($_SESSION['authenticated']) && $_SESSION['authenticated'] == true) {
                                                    echo htmlspecialchars($_SESSION['username']);
                                                }?> "placeholder="Pseudo"/> <input type="hidden" name="articleId" value="<?php echo htmlspecialchars($news->id()) ?>"/> <br/> <textarea type="text" name="content" class="content" rows="3" cols="50" placeholder="Réponse au commentaire"></textarea> <br /> <input type="submit" value="Commenter" class="submitComment" /></form> </div>';
                                                var div = document.getElementById("commentMain_<?php echo htmlspecialchars($comments['commentId']); ?>");
                                                insertAfter(div, el);
                                            }
                                        </script>

                                        <ul class="commentsList replyList">
                                            <?php foreach ($comment as $child) {
                                                if ($child['commentParent'] == $comments['commentId']) { ?>

                                                    <!-- Comment Enfant - niveau 1 -->
                                                    <li class="levelOne">
                                                        <div id="commentMain_<?php echo htmlspecialchars($child['commentId']); ?>"
                                                             class="commentMain">
                                                            <div class="commentAvatar">
                                                                <img src="/app_mvc/web/img/default_user.png" class="img-fluid"/>
                                                            </div>
                                                            <div class="commentBox">
                                                                <div class="commentHead">
                                                                    <h6 class="commentName"><a
                                                                                href="#"><?php echo htmlspecialchars($child['author']); ?></a>
                                                                    </h6>
                                                                    <span><?php $newsDate = strtotime($child['date']); ?>
                                                                        <?php echo htmlspecialchars($newsDateFormat = date("j M Y", $newsDate)); ?></span>
                                                                    <div id="replyIcon_<?php echo htmlspecialchars($child['commentId']); ?>">
                                                                        <a role="button" data-toggle="collapse"
                                                                           href="#replyCommentForm_<?php echo htmlspecialchars($child['commentId']) ?>"
                                                                           aria-expanded="false"
                                                                           aria-controls="replyCommentForm_<?php echo htmlspecialchars($child['commentId']); ?>">
                                                                            <i class="fa fa-reply"></i>
                                                                        </a>
                                                                    </div>
                                                                    <a role="button"
                                                                       href="/app_mvc/home/addReport?commentId=<?php echo htmlspecialchars($child['commentId']) ?>&id=<?php echo $child['articleId'] ?>">
                                                                        <i class="fa fa-flag"></i>
                                                                    </a>
                                                                </div>
                                                                <div class="commentContent">
                                                                    <?php echo strip_tags($child['content']) ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <script>
                                                        document.getElementById("replyIcon_<?php echo htmlspecialchars($child['commentId']); ?>").onclick = function (e) {
                                                            e = e || event;
                                                            var target = e.target || e.srcElement;
                                                            innerId = target.id;

                                                            function insertAfter(referenceNode, newNode) {
                                                                referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
                                                            }

                                                            $(document.getElementById("replyIcon_<?php echo htmlspecialchars($child['commentId']); ?>")).on('click', function () {
                                                                $('.collapse').collapse('hide');
                                                            });

                                                            var el = document.createElement("div");
                                                            el.innerHTML = '<div class="collapse replyChildPad" id="replyCommentForm_<?php echo htmlspecialchars($child['commentId']); ?>"> <form class="addCommentReply" action="/app_mvc/home/addComment?id=<?php echo htmlspecialchars($news->id()) ?>&commentParent=<?php echo htmlspecialchars($child['commentId']); ?>" method="post">  <input type="text" name="author" class="author" value="<?php if (isset($_SESSION['authenticated']) && $_SESSION['authenticated'] == true) {
                                                                echo htmlspecialchars($_SESSION['username']);
                                                            }?> "placeholder="Pseudo"/> <input type="hidden" name="articleId" value="<?php echo htmlspecialchars($news->id()) ?>"/> <br/> <textarea type="text" name="content" class="content" rows="3" cols="50" placeholder="Réponse au commentaire"></textarea> <br /> <input type="submit" value="Commenter" class="submitComment" /></form> </div>';
                                                            var div = document.getElementById("commentMain_<?php echo htmlspecialchars($child['commentId']); ?>");
                                                            insertAfter(div, el);
                                                        }
                                                    </script>


                                                <?php foreach ($comment as $child2) {
                                                if ($child2['commentParent'] == $child['commentId']) { ?>

                                                    <!-- Comment Enfant - niveau 2 -->
                                                    <li class="levelTwo">
                                                        <div id="commentMain_<?php echo htmlspecialchars($child2['commentId']); ?>"
                                                             class="commentMain">
                                                            <div class="commentAvatar">
                                                                <img src="/app_mvc/web/img/default_user.png" class="img-fluid"/>
                                                            </div>
                                                            <div class="commentBox">
                                                                <div class="commentHead">
                                                                    <h6 class="commentName"><a
                                                                                href="#"><?php echo htmlspecialchars($child2['author']); ?></a>
                                                                    </h6>
                                                                    <span><?php $newsDate = strtotime($child2['date']); ?>
                                                                        <?php echo htmlspecialchars($newsDateFormat = date("j M Y", $newsDate)); ?></span>
                                                                    <div id="replyIcon_<?php echo htmlspecialchars($child2['commentId']); ?>">
                                                                        <a role="button" data-toggle="collapse"
                                                                           href="#replyCommentForm_<?php echo htmlspecialchars($child2['commentId']) ?>"
                                                                           aria-expanded="false"
                                                                           aria-controls="replyCommentForm_<?php echo htmlspecialchars($child2['commentId']); ?>">
                                                                            <i class="fa fa-reply"></i>
                                                                        </a>
                                                                    </div>
                                                                    <a role="button"
                                                                       href="/app_mvc/home/addReport?commentId=<?php echo htmlspecialchars($child2['commentId']) ?>&id=<?php echo $child2['articleId'] ?>">
                                                                        <i class="fa fa-flag"></i>
                                                                    </a>
                                                                </div>
                                                                <div class="commentContent">
                                                                    <?php echo strip_tags($child2['content']) ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <script>
                                                        document.getElementById("replyIcon_<?php echo htmlspecialchars($child2['commentId']); ?>").onclick = function (e) {
                                                            e = e || event;
                                                            var target = e.target || e.srcElement;
                                                            innerId = target.id;

                                                            function insertAfter(referenceNode, newNode) {
                                                                referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
                                                            }

                                                            $(document.getElementById("replyIcon_<?php echo htmlspecialchars($child2['commentId']); ?>")).on('click', function () {
                                                                $('.collapse').collapse('hide');
                                                            });

                                                            var el = document.createElement("div");
                                                            el.innerHTML = '<div class="collapse replyChildPad" id="replyCommentForm_<?php echo htmlspecialchars($child2['commentId']); ?>"> <form class="addCommentReply" action="/app_mvc/home/addComment?id=<?php echo htmlspecialchars($news->id()) ?>&commentParent=<?php echo htmlspecialchars($child2['commentId']); ?>" method="post">  <input type="text" name="author" class="author" value="<?php if (isset($_SESSION['authenticated']) && $_SESSION['authenticated'] == true) {
                                                                echo htmlspecialchars($_SESSION['username']);
                                                            }?> "placeholder="Pseudo"/> <input type="hidden" name="articleId" value="<?php echo htmlspecialchars($news->id()) ?>"/> <br/> <textarea type="text" name="content" class="content" rows="3" cols="50" placeholder="Réponse au commentaire"></textarea> <br /> <input type="submit" value="Commenter" class="submitComment" /></form> </div>';
                                                            var div = document.getElementById("commentMain_<?php echo htmlspecialchars($child2['commentId']); ?>");
                                                            insertAfter(div, el);
                                                        }
                                                    </script>

                                                <?php foreach ($comment as $child3) {
                                                if ($child3['commentParent'] == $child2['commentId']) { ?>

                                                    <!-- Comment Enfant - niveau 3 -->
                                                    <li class="levelThree">
                                                        <div id="commentMain_<?php echo htmlspecialchars($child3['commentId']); ?>"
                                                             class="commentMain">
                                                            <div class="commentAvatar">
                                                                <img src="/app_mvc/web/img/default_user.png" class="img-fluid"/>
                                                            </div>
                                                            <div class="commentBox">
                                                                <div class="commentHead">
                                                                    <h6 class="commentName"><a
                                                                                href="#"><?php echo htmlspecialchars($child3['author']); ?></a>
                                                                    </h6>
                                                                    <span><?php $newsDate = strtotime($child3['date']); ?>
                                                                        <?php echo htmlspecialchars($newsDateFormat = date("j M Y", $newsDate)); ?></span>
                                                                    <a role="button"
                                                                       href="/app_mvc/home/addReport?commentId=<?php echo htmlspecialchars($child3['commentId']) ?>&id=<?php echo $child3['articleId'] ?>">
                                                                        <i class="fa fa-flag"></i>
                                                                    </a>
                                                                </div>
                                                                <div class="commentContent">
                                                                    <?php echo strip_tags($child3['content']) ?>
                                                                </div>
                                                            </div>
                                                    </li>
                                                <?php }
                                                } ?> <!-- Fin niveau 3 -->
                                                <?php }
                                                } ?> <!-- Fin niveau 2 -->
                                                <?php }
                                            } ?> <!-- Fin niveau 1 -->
                                        </ul>
                                    </li>
                                <?php }
                            } ?> <!-- Fin niveau 0 -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer">
            <span>© <?php echo date("Y"); ?> Copyright - <?php echo Config::get("siteName"); ?></span>
        </footer>

    </div> <!-- mainContainer -->
</body>
</html>
