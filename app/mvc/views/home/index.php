<?php
require_once(TEMP . DS . "headerHome.php");
require_once(TEMP . DS . "navbar.php");
?>

        <section id="news">
            <div class="homeNews">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <?php
                                foreach ($data as $news) {
                                ?>
                                <div class="col-md-6">
                                    <div class="newsSingle">
                                        <a href='/app_mvc/home/show?id=<?php echo htmlspecialchars($news->id()); ?>'>
                                            <div class="newsImage">
                                                <img src="/app_mvc/web/img/news/<?php echo $news->image() ?>" class="img-fluid" />
                                            </div>
                                            <div class="newsContainer">
                                                <div class="newsTitle"><?php $title = strip_tags($news->title());
                                                    if (strlen($title) > 10) {
                                                        $stringCut = substr($title, 0, 50);
                                                        $title = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
                                                    } ?>
                                                    <p><?php echo $title; ?></p>
                                                </div>
                                                <div class="newsDate"><?php $newsDate = strtotime($news->dateAdd()); ?>
                                                    <em><?php echo htmlspecialchars($newsDateFormat = date("j M Y", $newsDate)); ?></em>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                    <?php
                                }
                                ?>
                            </div>

                            <?php
                            foreach ($lastNews as $news) {
                            ?>
                            <div class="col-md-6">
                                <div class="lastNewsSingle">
                                    <div class="newsImage">
                                        <img src="/app_mvc/web/img/news/<?php echo $news->image() ?>" class="img-fluid" />
                                    </div>
                                    <div class="newsTitle"><?php echo htmlspecialchars($news->title()) ?></div>
                                    <div class="newsMeta">
                                        <div class="lastNewsAuthor">
                                            <i class="fa fa-user-o" aria-hidden="true"></i>
                                            <?php echo htmlspecialchars($news->author()) ?>
                                        </div>
                                        <div class="lastNewsDate">
                                            <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                            <?php $newsDate = strtotime($news->dateAdd()); ?>
                                            <?php echo htmlspecialchars($newsDateFormat = date("j M Y", $newsDate)); ?>
                                        </div>
                                    </div>
                                    <div class="newsContent"><?php $content = strip_tags($news->content());
                                                                if (strlen($content) > 450) {
                                                                    $stringCut = substr($content, 0, 450);
                                                                    $content = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
                                                                } ?>
                                        <p><?php echo $content; ?></p> <div class="readMore"><a href="/app_mvc/home/show?id=<?php echo $news->id()?>">Lire la suite</a></div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="about">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2 class="aboutHeader" id="about">À propos</h2>
                        <h3 class="aboutSubHeader text-muted"><em>Lorem ipsum dolor sit amet consectetur.</em></h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <ul class="timeline">
                            <li data-aos="fade-right" data-aos-once="true">
                                    <div class="timelineImage">
                                        <img class="img-circle img-responsive" src="/app_mvc/web/img/3_thumb.jpg" alt="">
                                    </div>
                                    <div class="timelinePanel">
                                        <div class="timelineHeader">
                                            <h4>2007</h4>
                                            <h4 class="subHeader">Début de carrière</h4>
                                        </div>
                                        <div class="timelineBody">
                                            <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                        </div>
                                    </div>
                            </li>
                            <li class="timelineInverted" data-aos="fade-left" data-aos-once="true">
                                <div class="timelineImage">
                                    <img class="img-circle img-responsive" src="/app_mvc/web/img/5_thumb.jpg" alt="">
                                </div>
                                <div class="timelinePanel">
                                    <div class="timelineHeader">
                                        <h4>Novembre 2009</h4>
                                        <h4 class="subHeader">Première parution</h4>
                                    </div>
                                    <div class="timelineBody">
                                        <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                    </div>
                                </div>
                            </li>
                            <li data-aos="fade-right" data-aos-once="true">
                                <div class="timelineImage">
                                    <img class="img-circle img-responsive" src="/app_mvc/web/img/a-3.jpg" alt="">
                                </div>
                                <div class="timelinePanel">
                                    <div class="timelineHeader">
                                        <h4>2009 - 2016</h4>
                                        <h4 class="subHeader">Autres romans</h4>
                                    </div>
                                    <div class="timelineBody">
                                        <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                    </div>
                                </div>
                            </li>
                            <li class="timelineInverted" data-aos="fade-left" data-aos-once="true">
                                <div class="timelineImage">
                                    <img class="img-circle img-responsive" src="/app_mvc/web/img/a-4.jpg" alt="">
                                </div>
                                <div class="timelinePanel">
                                    <div class="timelineHeader">
                                        <h4>2017</h4>
                                        <h4 class="subHeader">Billet simple pour l'Alaska</h4>
                                    </div>
                                    <div class="timelineBody">
                                        <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                    </div>
                                </div>
                            </li>
                            <li class="timelineInverted" data-aos="fade-up" data-aos-once="true">
                                <div class="timelineImage">
                                    <h4><br/>A suivre...</h4>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section id="bio">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2 class="bioHeader" id="about">Biographie</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="bioImage" data-aos="fade-down-right" data-aos-once="true">
                                <img src="/app_mvc/web/img/bio.jpg"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="bioText">
                                <p>Sed a neque mi. Cras eu semper est, et consectetur diam. Etiam et nibh sit amet eros sagittis finibus. Ut et felis vitae odio aliquet porta vel in lacus. Duis porttitor neque at sapien varius, quis tempor magna vulputate. Fusce et sem felis. Nunc nulla ligula, accumsan et justo volutpat, tempus pulvinar dui. Vivamus id arcu ut felis dignissim fringilla sed non nisl. Etiam volutpat ante lectus, nec dignissim dolor lacinia ac. </p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="bioText">
                                <p>Curabitur commodo, diam ac vehicula condimentum, nisi lacus scelerisque massa, vitae posuere nisi massa ac felis. Vivamus at urna nunc. Praesent aliquam accumsan ipsum, nec tempus felis euismod quis. Ut arcu nibh, fermentum quis dolor nec, sodales facilisis nibh. Aliquam diam velit, suscipit ac dignissim sed, pretium vel velit. Duis sodales mauris ipsum, quis dignissim lectus blandit in. Nunc nibh urna.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <section id="presse">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2 class="presseHeader" id="about">Dans la presse</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="col-md-4">
                            <div class="quoteIco">
                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                            </div>
                            <div class="quoteContent">
                                Donec imperdiet diam quis mi hendrerit imperdiet. Nulla vel ipsum justo. Integer dapibus ex ac purus euismod pretium. Curabitur aliquam nisi nunc, in condimentum risus venenatis eget. Mauris ut vehicula erat.
                            </div>
                            <div class="quoteImage" data-aos="zoom-in" data-aos-once="true">
                                <img src="/app_mvc/web/img/quote-1" class="img-fluid" />
                            </div>
                            <div class="quoteAuthor">
                                <em><strong>JAKE RANDELL</strong>,<br/> THE WASHINGTON PAPER</em>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="quoteIco">
                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                            </div>
                            <div class="quoteContent">
                                Donec imperdiet diam quis mi hendrerit imperdiet. Nulla vel ipsum justo. Integer dapibus ex ac purus euismod pretium. Curabitur aliquam nisi nunc, in condimentum risus venenatis eget. Mauris ut vehicula erat.
                            </div>
                            <div class="quoteImage" data-aos="zoom-in" data-aos-once="true">
                                <img src="/app_mvc/web/img/quote-2" class="img-fluid" />
                            </div>
                            <div class="quoteAuthor">
                                <em><strong>GREG FERNANDEZ</strong>, <br/> SEATTLE DAILY</em>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="quoteIco">
                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                            </div>
                            <div class="quoteContent">
                                Donec imperdiet diam quis mi hendrerit imperdiet. Nulla vel ipsum justo. Integer dapibus ex ac purus euismod pretium. Curabitur aliquam nisi nunc, in condimentum risus venenatis eget. Mauris ut vehicula erat.
                            </div>
                            <div class="quoteImage" data-aos="zoom-in" data-aos-once="true">
                                <img src="/app_mvc/web/img/quote-3" class="img-fluid" />
                            </div>
                            <div class="quoteAuthor">
                                <em><strong>GEMMA NASHVILLE</strong>, <br/> BOOKS IN REVIEW</em>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2 class="contactHeader" id="about">Contact</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="contact">Pour toute information ou réclamation, contactez <strong>Jean Forteroche</strong> :</div>
                        <div class="infos"><strong>Tel</strong>: 123-456-7890   <b>|</b>   <strong>Fax</strong>: 123-456-7890    <b>|</b>    <strong>Email</strong>:  j.forteroche@gmail.com</div>
                        <div class="social">Retrouvez-moi :
                            <span class="socialFb"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></span>
                            <span class="socialTwitter"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></span>
                            <span class="socialLinkedIn"><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></span>
                            <span class="socialPinterest"><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></span>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer class="footer">
            <span>© <?php echo htmlspecialchars(date("Y"));?> Copyright - <?php echo htmlspecialchars(Config::get("siteName")); ?></span>
        </footer>

    </div> <!-- mainContainer -->
</body>
</html>