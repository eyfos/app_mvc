<?php
/**
 * Created by PhpStorm.
 * User: Saiya
 * Date: 5/11/2017
 * Time: 12:30 PM
 */

class Comment extends Entity {

    protected $articleId;
    protected $commentId;
    protected $parentId;
    protected $author;
    protected $content;
    protected $date;
    protected $report;

    const AUTEUR_INVALIDE = 1;
    const CONTENU_INVALIDE = 2;


    // SETTERS //

    public function setAuthor($author){
        if (!is_string($author) || empty($author))
        {
            $this->erreurs[] = self::AUTEUR_INVALIDE;
        }

        $this->author = $author;
    }

    public function setContent($content) {
        if (!is_string($content) || empty($content))
        {
            $this->erreurs[] = self::CONTENU_INVALIDE;
        }

        $this->content = $content;
    }

    public function setCommentId($commentId) {
        $this->commentId = $commentId;
    }

    public function setArticleId($articleId){
        $this->articleId = $articleId;
    }

    public function setParentId($parentId) {
        $this->parentId = $parentId;
    }

    public function setDate($date) {
        $this->date = $date;
    }

    public function setReport($report) {
        $this->report = $report;
    }

    // GETTERS //

    public function commentId() {
        return $this->commentId;
    }

    public function author() {
        return $this->author;
    }

    public function content() {
        return $this->content;
    }

    public function date() {
        return $this->date;
    }

    public function articleId() {
        return $this->articleId;
    }

    public function parentId() {
        return $this->parentId;
    }

    public function report() {
        return $this->report;
    }
}