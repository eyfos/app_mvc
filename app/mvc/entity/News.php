<?php
/**
 * Created by PhpStorm.
 * User: Saiya
 * Date: 5/3/2017
 * Time: 04:22 PM
 */

class News extends Entity {

    protected $id;
    protected $author;
    protected $image;
    protected $title;
    protected $content;
    protected $dateAdd;
    protected $dateModif;

    const AUTEUR_INVALIDE = 1;
    const TITRE_INVALIDE = 2;
    const CONTENU_INVALIDE = 3;

    public function setAuthor($author) {
        if (!is_string($author) || empty($author)) {
            $this->erreurs[] = self::AUTEUR_INVALIDE;
        }

        $this->author = $author;
    }

    public function setImage($image) {
        if (isset($image['name'])) {
            $imgFile = $image['name'];
            $tmp_dir = $image['tmp_name'];
            $imgSize = $image['size'];

            $upload_dir = NEWS_IMG.DS;

            $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION));

            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif');

            $newImage = rand(1000,1000000).".".$imgExt;

            if(in_array($imgExt, $valid_extensions)) {
                if($imgSize < 5000000) {
                    move_uploaded_file($tmp_dir,$upload_dir.$newImage);
                    $this->image = $newImage;
                } else{
                    $errMSG = "Sorry, your file is too large.";
                }
            } else {
                $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            }
        } else {
            $this->image = $image;
        }
    }

    public function setTitle($title) {
        if (!is_string($title) || empty($title)) {
            $this->erreurs[] = self::TITRE_INVALIDE;
        }

        $this->title = $title;
    }

    public function setContent($content) {
        if (!is_string($content) || empty($content)) {
            $this->erreurs[] = self::CONTENU_INVALIDE;
        }

        $this->content = $content;
    }

    public function setDateAdd($dateAdd) {
        $this->dateAdd = $dateAdd;
    }

    public function setDateModif($dateModif) {
        $this->dateModif = $dateModif;
    }

    // GETTERS //

    public function author() {
        return $this->author;
    }

    public function image() {
        return $this->image;
    }

    public function title() {
        return $this->title;
    }

    public function content() {
        return $this->content;
    }

    public function dateAdd() {
        return $this->dateAdd;
    }

    public function dateModif() {
        return $this->dateModif;
    }

    public function id() {
        return $this->id;
    }
}
