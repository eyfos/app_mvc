<?php
/**
 * Created by PhpStorm.
 * User: Saiya
 * Date: 5/11/2017
 * Time: 01:49 PM
 */

class User extends Entity {

    protected $id;
    protected $username;
    protected $password;
    protected $image;

    const NOM_INVALIDE = 1;
    const PASSWORD_INVALIDE = 2;

    // SETTERS //

    public function setUsername($username) {
        if (!is_string($username) || empty($username))
        {
            $this->erreurs[] = self::NOM_INVALIDE;
        }

        $this->username = $username;
    }

    public function setPassword($password) {
        if (!is_string($password) || empty($password))
        {
            $this->erreurs[] = self::PASSWORD_INVALIDE;
        }

        $this->password = $password;
    }

    public function setImage($image) {
        if (isset($image['name'])) {
            $imgFile = $image['name'];
            $tmp_dir = $image['tmp_name'];
            $imgSize = $image['size'];

            $upload_dir = USERS_IMG.DS;

            $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION));

            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif');

            $newImage = rand(1000,1000000).".".$imgExt;

            if(in_array($imgExt, $valid_extensions)) {
                if($imgSize < 5000000) {
                    move_uploaded_file($tmp_dir,$upload_dir.$newImage);
                    $this->image = $newImage;
                } else{
                    $errMSG = "Sorry, your file is too large.";
                }
            } else {
                $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            }
        } else {
            $this->image = $image;
        }
    }

    public function updateImage($image) {
        $imgFile = $_FILES['image']['name'];
        $tmp_dir = $_FILES['image']['tmp_name'];
        $imgSize = $_FILES['image']['size'];

        if($imgFile) {
            $upload_dir = USERS_IMG.DS;

            $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION));

            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif');

            $newImage = rand(1000,1000000).".".$imgExt;

            if(in_array($imgExt, $valid_extensions)) {
                if($imgSize < 5000000) {
                    unlink($upload_dir.$newImage);
                    move_uploaded_file($tmp_dir,$upload_dir.$newImage);
                }
                else {
                    $errMSG = "Sorry, your file is too large it should be less than 5MB";
                }
            } else {
                $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            }
        } else {
            $this->image = $image;
        }
    }

    // GETTERS //

    public function id() {
        return $this->id;
    }

    public function username() {
        return $this->username;
    }

    public function password() {
        return $this->password;
    }

    public function image() {
        return $this->image;
    }
}